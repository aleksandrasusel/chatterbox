import 'package:bloc_test/bloc_test.dart';
import 'package:chatterbox/domain/use_case/authentication_use_case/get_user_profile_use_case.dart';
import 'package:chatterbox/domain/use_case/authentication_use_case/sign_out_use_case.dart';
import 'package:chatterbox/domain/utils/failure.dart';
import 'package:chatterbox/domain/utils/success.dart';
import 'package:chatterbox/presentation/bloc/app_bloc.dart';
import 'package:chatterbox/presentation/utils/enums/errors.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

import '../../mocks.dart';
import '../../mocks_data.dart';

void main() {
  late GetUserProfileUseCase mockedGetUserProfileUseCase;
  late SignOutUseCase mockedSignOutUseCase;

  setUpAll(() {
    mockedGetUserProfileUseCase = MockGetUserProfileUseCase();
    mockedSignOutUseCase = MockSignOutUseCase();
  });

  AppBloc createBloc() => AppBloc(mockedGetUserProfileUseCase, mockedSignOutUseCase);

  // test('InitializeAppEvent fetch user data', () async {
  //   /// 1. setup
  //   when(() => mockedGetUserProfileUseCase()).thenAnswer((_) async => const Right(mockedUserProfileEntity));
  //
  //   /// 2. act
  //
  //   final blocTest = createBloc()..add(const OnInitializeAppEvent());
  //   await pumpEventQueue();
  //
  //   /// 3.Expect
  //   expect(blocTest.state, const AppState.initial('Janusz'));
  //   emitsInOrder([
  //     const AppState.loading(),
  //     const AppState.initial(''),
  //   ]);
  //
  //   verify(() => mockedGetUserProfileUseCase());
  //   verifyNoMoreInteractions(mockedGetUserProfileUseCase);
  // });

  blocTest<AppBloc, AppState>(
    'InitializeAppEvent fetch user data successful',
    setUp: () {
      when(() => mockedGetUserProfileUseCase()).thenAnswer((_) async => const Right(mockedUserProfileEntity));
    },
    build: createBloc,
    act: (bloc) => bloc.add(const OnInitializeAppEvent()),
    verify: (_) {
      verify(() => mockedGetUserProfileUseCase());
      verifyNoMoreInteractions(mockedGetUserProfileUseCase);
    },
    expect: () => [const AppState.initial('Janusz')],
  );

  blocTest<AppBloc, AppState>(
    'InitializeAppEvent fetch user data failure',
    setUp: () {
      when(() => mockedGetUserProfileUseCase()).thenAnswer((_) async => const Left(Failure(Errors.unknownError)));
    },
    build: createBloc,
    act: (bloc) => bloc.add(const OnInitializeAppEvent()),
    verify: (_) {
      verify(() => mockedGetUserProfileUseCase());
      verifyNoMoreInteractions(mockedGetUserProfileUseCase);
    },
    expect: () => [const AppState.initial('Nazwa niedostępna')],
  );

  blocTest<AppBloc, AppState>(
    'OnCheckUserEvent checks if user is exist successful',
    setUp: () {
      when(() => mockedGetUserProfileUseCase()).thenAnswer((_) async => const Right(mockedUserProfileEntity));
    },
    build: createBloc,
    act: (bloc) => bloc.add(const OnCheckUserEvent()),
    verify: (_) {
      verify(() => mockedGetUserProfileUseCase());
      verifyNoMoreInteractions((mockedGetUserProfileUseCase));
    },
    expect: () => [const AppState.toHomePage()],
  );

  blocTest<AppBloc, AppState>(
    'OnCheckUserEvent checks if user is exist failure',
    setUp: () {
      when(() => mockedGetUserProfileUseCase()).thenAnswer((_) async => const Left(Failure(Errors.unknownError)));
    },
    build: createBloc,
    act: (bloc) => bloc.add(const OnCheckUserEvent()),
    verify: (_) {
      verify(() => mockedGetUserProfileUseCase());
      verifyNoMoreInteractions((mockedGetUserProfileUseCase));
    },
    expect: () => [const AppState.toIntroPage()],
  );

  blocTest<AppBloc, AppState>(
    'OnSignOutEvent sign out the user successful',
    setUp: () {
      when(() => mockedSignOutUseCase()).thenAnswer((_) async => const Right(Success()));
    },
    build: createBloc,
    act: (bloc) => bloc.add(const OnSignOutEvent()),
    verify: (_) {
      verify(() => mockedSignOutUseCase());
      verifyNoMoreInteractions((mockedSignOutUseCase));
    },
    expect: () => [const AppState.toIntroPage()],
  );

  blocTest<AppBloc, AppState>(
    'OnSignOutEvent sign out the user failure',
    setUp: () {
      when(() => mockedSignOutUseCase()).thenAnswer((_) async => const Left(Failure(Errors.unknownError)));
    },
    build: createBloc,
    act: (bloc) => bloc.add(const OnSignOutEvent()),
    verify: (_) {
      verify(() => mockedSignOutUseCase());
      verifyNoMoreInteractions((mockedSignOutUseCase));
    },
    expect: () => [const AppState.failure(Errors.unknownError)],
  );
}
