import 'package:bloc_test/bloc_test.dart';
import 'package:chatterbox/domain/use_case/community_use_case/add_friend_use_case.dart';
import 'package:chatterbox/domain/use_case/community_use_case/get_users_use_case.dart';
import 'package:chatterbox/domain/utils/failure.dart';
import 'package:chatterbox/domain/utils/success.dart';
import 'package:chatterbox/presentation/pages/search_friends_page/bloc/search_friends_bloc.dart';
import 'package:chatterbox/presentation/pages/search_friends_page/bloc/search_friends_event.dart';
import 'package:chatterbox/presentation/pages/search_friends_page/bloc/search_friends_state.dart';
import 'package:chatterbox/presentation/utils/enums/errors.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

import '../../../mocks.dart';
import '../../../mocks_data.dart';

void main() {
  late GetUsersUseCase mockedGetUsersUseCase;
  late AddFriendUseCase mockedAddFriendUseCase;

  setUpAll(() {
    mockedGetUsersUseCase = MockGetUsersUseCase();
    mockedAddFriendUseCase = MockAddFriendUseCase();
  });

  SearchFriendsBloc createBloc() => SearchFriendsBloc(mockedGetUsersUseCase, mockedAddFriendUseCase);

  blocTest<SearchFriendsBloc, SearchFriendsState>(
    'OnInitializeEvent to fetch a friends list successful',
    setUp: () {
      when(() => mockedGetUsersUseCase.call()).thenAnswer((_) async => Right(mockedFriendEntityList));
    },
    build: createBloc,
    act: (bloc) => bloc.add(
      const SearchFriendsInitializeEvent(),
    ),
    expect: () => [
      SearchFriendsState.initial(mockedFriendEntityList),
    ],
    verify: (_) {
      verify(() => mockedGetUsersUseCase.call()).called(1);
    },
  );

  blocTest<SearchFriendsBloc, SearchFriendsState>(
    'OnInitializeEvent to fetch a friends list failure',
    setUp: () {
      when(() => mockedGetUsersUseCase.call()).thenAnswer((_) async => const Left(Failure(Errors.somethingWentWrong)));
    },
    build: createBloc,
    act: (bloc) => bloc.add(
      const SearchFriendsInitializeEvent(),
    ),
    expect: () => [
      const SearchFriendsState.failure(Errors.somethingWentWrong),
    ],
    verify: (_) {
      verify(() => mockedGetUsersUseCase.call()).called(1);
    },
  );

  blocTest<SearchFriendsBloc, SearchFriendsState>(
    'AddFriendEvent to add a friend to friends list failure',
    setUp: () {
      when(() => mockedAddFriendUseCase.send(mockedFriedEntity3))
          .thenAnswer((_) async => const Left(Failure(Errors.somethingWentWrong)));
    },
    build: createBloc,
    act: (bloc) => bloc.add(
      const AddFriendEvent(mockedFriedEntity3),
    ),
    expect: () => [
      const SearchFriendsState.failure(Errors.somethingWentWrong),
    ],
    verify: (_) {
      verify(() => mockedAddFriendUseCase.send(mockedFriedEntity3)).called(1);
    },
  );

  blocTest<SearchFriendsBloc, SearchFriendsState>(
    'AddFriendEvent to add a friend to friends list successful',
    setUp: () {
      when(() => mockedAddFriendUseCase.send(mockedFriedEntity3)).thenAnswer((_) async => const Right(Success()));
      when(() => mockedGetUsersUseCase()).thenAnswer((_) async => Right(mockedFriendEntityList));
    },
    build: createBloc,
    act: (bloc) => bloc.add(const AddFriendEvent(mockedFriedEntity3)),
    expect: () => [
      const SearchFriendsState.success(),
      SearchFriendsState.initial(mockedFriendEntityList),
    ],
    verify: (_) {
      verify(() => mockedAddFriendUseCase.send(mockedFriedEntity3));
      verify(() => mockedGetUsersUseCase.call());
      verifyNoMoreInteractions(mockedAddFriendUseCase);
      verifyNoMoreInteractions(mockedGetUsersUseCase);
    },
  );
}
