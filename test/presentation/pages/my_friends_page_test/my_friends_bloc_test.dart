import 'package:bloc_test/bloc_test.dart';
import 'package:chatterbox/domain/use_case/community_use_case/delete_friend_use_case.dart';
import 'package:chatterbox/domain/use_case/community_use_case/get_friends_list_use_case.dart';
import 'package:chatterbox/domain/utils/failure.dart';
import 'package:chatterbox/presentation/pages/my_friends_page/bloc/my_friends_bloc.dart';
import 'package:chatterbox/presentation/pages/my_friends_page/bloc/my_friends_event.dart';
import 'package:chatterbox/presentation/pages/my_friends_page/bloc/my_friends_state.dart';
import 'package:chatterbox/presentation/utils/enums/errors.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

import '../../../mocks.dart';
import '../../../mocks_data.dart';

void main() {
  late GetFriendsListUseCase mockedGetFriendsListUseCase;
  late DeleteFriendUseCase mockedDeleteFriendUseCase;

  setUpAll(() {
    mockedGetFriendsListUseCase = MockGetFriendsListUseCase();
    mockedDeleteFriendUseCase = MockDeleteFriendUseCase();
  });

  MyFriendsBloc createBloc() => MyFriendsBloc(mockedGetFriendsListUseCase, mockedDeleteFriendUseCase);

  blocTest<MyFriendsBloc, MyFriendsState>(
    'InitializeEvent to fetch friends list successful',
    setUp: () {
      when(() => mockedGetFriendsListUseCase.call()).thenAnswer((_) async => Right(mockedFriendEntityList));
    },
    build: createBloc,
    act: (bloc) => bloc.add(const MyFriendsInitializeEvent()),
    expect: () => [
      MyFriendsState.loaded(mockedFriendEntityList),
    ],
    verify: (_) {
      verify(() => mockedGetFriendsListUseCase.call()).called(1);
      verifyNoMoreInteractions(mockedGetFriendsListUseCase);
    },
  );

  blocTest<MyFriendsBloc, MyFriendsState>(
    'InitializeEvent to fetch friends list failure',
    setUp: () {
      when(() => mockedGetFriendsListUseCase.call()).thenAnswer((_) async => const Left(Failure(Errors.somethingWentWrong)));
    },
    build: createBloc,
    act: (bloc) => bloc.add(const MyFriendsInitializeEvent()),
    expect: () => [
      const MyFriendsState.failure(Errors.somethingWentWrong),
    ],
    verify: (_) {
      verify(() => mockedGetFriendsListUseCase.call()).called(1);
      verifyNoMoreInteractions(mockedGetFriendsListUseCase);
    },
  );

  blocTest<MyFriendsBloc, MyFriendsState>(
    'DeleteFiendEvent to fetch friends list successful',
    setUp: () {
      when(() => mockedDeleteFriendUseCase.call(1)).thenAnswer((_) async => Right(mockedFriendEntityList));
    },
    build: createBloc,
    act: (bloc) => bloc.add(const DeleteFriendEvent(1)),
    expect: () => [
      const MyFriendsState.success(),
      MyFriendsState.loaded(mockedFriendEntityList),
    ],
    verify: (_) {
      verify(() => mockedDeleteFriendUseCase.call(1)).called(1);
      verifyNoMoreInteractions(mockedDeleteFriendUseCase);
    },
  );

  blocTest<MyFriendsBloc, MyFriendsState>(
    'DeleteFiendEvent to fetch friends list failure',
    setUp: () {
      when(() => mockedDeleteFriendUseCase.call(1)).thenAnswer((_) async => const Left(Failure(Errors.somethingWentWrong)));
    },
    build: createBloc,
    act: (bloc) => bloc.add(
      const DeleteFriendEvent(1),
    ),
    verify: (_) {
      verify(() => mockedDeleteFriendUseCase.call(1)).called(1);
      verifyNoMoreInteractions(mockedDeleteFriendUseCase);
    },
    expect: () => [const MyFriendsState.failure(Errors.somethingWentWrong)],
  );
}
