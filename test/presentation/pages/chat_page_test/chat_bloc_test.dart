import 'package:bloc_test/bloc_test.dart';
import 'package:chatterbox/domain/use_case/chat_use_case/get_messages_use_case.dart';
import 'package:chatterbox/domain/use_case/chat_use_case/send_message_use_case.dart';
import 'package:chatterbox/domain/utils/success.dart';
import 'package:chatterbox/presentation/pages/chat_page/bloc/chat_bloc.dart';
import 'package:chatterbox/presentation/pages/chat_page/bloc/chat_event.dart';
import 'package:chatterbox/presentation/pages/chat_page/bloc/chat_state.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

import '../../../mocks.dart';
import '../../../mocks_data.dart';

void main() {
  late SendMessageUseCase mockedSendMessageUseCase;
  late GetMessagesUseCase mockedGetMessagesUseCase;

  setUpAll(() {
    mockedSendMessageUseCase = MockSendMessageUseCase();
    mockedGetMessagesUseCase = MockGetMessagesUseCase();
  });

  ChatBloc createBloc() => ChatBloc(mockedSendMessageUseCase, mockedGetMessagesUseCase);

  blocTest<ChatBloc, ChatState>(
    'SendMessageEvent to send a message successful',
    setUp: () {
      when(() => mockedSendMessageUseCase.send(sendMessageParams)).thenAnswer((_) async => const Right(Success()));
    },
    build: createBloc,
    act: (bloc) => bloc.add(SendMessageEvent(message: sendMessageParams.message, friendId: sendMessageParams.friendId)),
    verify: (_) {
      verify(() => mockedSendMessageUseCase.send(sendMessageParams));
      verifyNoMoreInteractions(mockedSendMessageUseCase);
    },
  );

  blocTest<ChatBloc, ChatState>(
    'InitializeEvent to fetch a messages in initial state successful',
    setUp: () {
      when(() => mockedGetMessagesUseCase.call('aaa')).thenAnswer((_) => Stream.value(mockedMessageEntityList));
    },
    build: createBloc,
    act: (bloc) => bloc.add(const InitializeChatEvent(friendId: 'aaa')),
    verify: (_) {
      verify(() {
        return mockedGetMessagesUseCase.call('aaa');
      });
      verifyNoMoreInteractions(mockedGetMessagesUseCase);
    },
  );
}

// tearDown(() {
//   chatBloc.close();
// });

// blocTest<ChatBloc, ChatState>(
//   'InitializeChatEvent fetch user data successful',
//   setUp: () {
//     final mockMessages = Stream.fromIterable([mockedMessageEntityList]);
//     when(() => mockedGetMessagesUseCase.call('aaa')).thenAnswer((_) => mockMessages);
//   },
//   build: createBloc,
//   act: (bloc) => bloc.add( const InitializeChatEvent(friendId: 'aaa')),
//
//
//   expect: () async {
//     await expectLater( , emitsInOrder(mockMessages));
//   },
//
//   verify: (_) {
//     verify(() => mockedGetMessagesUseCase('aaa'));
//     verifyNoMoreInteractions(mockedGetMessagesUseCase);
//   },
//
// );
