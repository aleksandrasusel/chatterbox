import 'package:bloc_test/bloc_test.dart';
import 'package:chatterbox/domain/use_case/authentication_use_case/login_use_case.dart';
import 'package:chatterbox/domain/utils/failure.dart';
import 'package:chatterbox/presentation/pages/login_page/bloc/login_bloc.dart';
import 'package:chatterbox/presentation/utils/enums/errors.dart';
import 'package:dartz/dartz.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

import '../../../mocks.dart';
import '../../../mocks_data.dart';

void main() {
  late LoginUseCase mockedLoginUseCase;
  late User mockedUser;

  setUpAll(() {
    mockedLoginUseCase = MockLoginUseCase();
    mockedUser = MockUser(uid: '1');
  });

  LoginBloc createBloc() => LoginBloc(mockedLoginUseCase);

  blocTest<LoginBloc, LoginState>(
    'OnTapLoginEvent to send a message successful',
    setUp: () {
      when(() => mockedLoginUseCase.call(mockedLoginEntity)).thenAnswer((_) async => Right(mockedUser));
    },
    build: createBloc,
    act: (bloc) => bloc.add(OnTapLoginEvent(password: mockedLoginEntity.password, email: mockedLoginEntity.email)),
    expect: () => [const LoginState.success()],
    verify: (_) {
      verify(() => mockedLoginUseCase.call(mockedLoginEntity)).called(1);
    },
  );
  blocTest<LoginBloc, LoginState>(
    'OnTapLoginEvent to send a message failure',
    setUp: () {
      when(() => mockedLoginUseCase.call(mockedLoginEntity)).thenAnswer((_) async => const Left(Failure(Errors.somethingWentWrong)));
    },
    build: createBloc,
    act: (bloc) => bloc.add(OnTapLoginEvent(password: mockedLoginEntity.password, email: mockedLoginEntity.email)),
    expect: () => [const LoginState.failure(Errors.somethingWentWrong)],
    verify: (_) {
      verify(() => mockedLoginUseCase.call(mockedLoginEntity)).called(1);
    },
  );
}
