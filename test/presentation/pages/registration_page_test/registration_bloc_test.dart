import 'package:bloc_test/bloc_test.dart';
import 'package:chatterbox/domain/use_case/authentication_use_case/registration_use_case.dart';
import 'package:chatterbox/domain/utils/failure.dart';
import 'package:chatterbox/domain/utils/success.dart';
import 'package:chatterbox/presentation/pages/registration_page/bloc/registration_bloc.dart';
import 'package:chatterbox/presentation/utils/enums/errors.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

import '../../../mocks.dart';
import '../../../mocks_data.dart';

void main() {
  late RegistrationUseCase mockedRegistrationUseCase;

  setUpAll(() {
    mockedRegistrationUseCase = MockRegistrationUseCase();
  });

  RegistrationBloc createBloc() => RegistrationBloc(mockedRegistrationUseCase);

  blocTest<RegistrationBloc, RegistrationState>(
    'OnTapLoginEvent to send a message successful',
    setUp: () {
      when(() => mockedRegistrationUseCase.call(mockedCreateUserEntity)).thenAnswer((_) async => const Right(Success()));
    },
    build: createBloc,
    act: (bloc) => bloc.add(
      OnTapRegistrationEvent(
        password: mockedCreateUserEntity.password,
        email: mockedCreateUserEntity.email,
        repeatPassword: mockedCreateUserEntity.password,
      ),
    ),
    expect: () => [
      const RegistrationState.loading(),
      const RegistrationState.success(),
    ],
    verify: (_) {
      verify(() => mockedRegistrationUseCase.call(mockedCreateUserEntity)).called(1);
    },
  );
  blocTest<RegistrationBloc, RegistrationState>(
    'OnTapLoginEvent to send a message failure',
    setUp: () {
      when(() => mockedRegistrationUseCase.call(mockedCreateUserEntity))
          .thenAnswer((_) async => const Left(Failure(Errors.somethingWentWrong)));
    },
    build: createBloc,
    act: (bloc) => bloc.add(
      OnTapRegistrationEvent(
        password: mockedCreateUserEntity.password,
        email: mockedCreateUserEntity.email,
        repeatPassword: mockedCreateUserEntity.password,
      ),
    ),
    expect: () => [
      const RegistrationState.failure(Errors.somethingWentWrong),
      const RegistrationState.initial(),
    ],
    verify: (_) {
      verify(() => mockedRegistrationUseCase.call(mockedCreateUserEntity)).called(1);
    },
  );
}
