import 'package:bloc_test/bloc_test.dart';
import 'package:chatterbox/domain/use_case/chat_use_case/get_chat_tile_list_use_case.dart';
import 'package:chatterbox/presentation/pages/home_page/bloc/home_page_bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

import '../../../mocks.dart';
import '../../../mocks_data.dart';

void main() {
  late GetChatTileListUseCase mockedGetChatTileListUseCase;

  setUpAll(() {
    mockedGetChatTileListUseCase = MockGetChatTileListUseCase();
  });

  HomePageBloc createBloc() => HomePageBloc(mockedGetChatTileListUseCase);

  blocTest<HomePageBloc, HomePageState>(
    'SendMessageEvent to send a message successful',
    setUp: () {
      when(() => mockedGetChatTileListUseCase.call()).thenAnswer((_) async => Right(mockedChatTileEntityList));
    },
    build: createBloc,
    act: (bloc) => bloc.add(const GetMessagesEvent()),
    verify: (_) {
      verify(() => mockedGetChatTileListUseCase.call());
      verifyNoMoreInteractions(mockedGetChatTileListUseCase);
    },
    expect: () => [HomePageState.initial(mockedChatTileEntityList)],
  );
}
