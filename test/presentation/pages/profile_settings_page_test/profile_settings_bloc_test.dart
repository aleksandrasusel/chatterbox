import 'package:bloc_test/bloc_test.dart';
import 'package:chatterbox/domain/use_case/authentication_use_case/change_password_use_case.dart';
import 'package:chatterbox/domain/use_case/authentication_use_case/user_profile_use_case.dart';
import 'package:chatterbox/domain/utils/failure.dart';
import 'package:chatterbox/domain/utils/success.dart';
import 'package:chatterbox/presentation/pages/profile_setting_page/profile_setting_bloc/profile_setting_bloc.dart';
import 'package:chatterbox/presentation/utils/enums/errors.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

import '../../../mocks.dart';
import '../../../mocks_data.dart';

void main() {
  late UserProfileUseCase mockedUserProfileUseCase;
  late ChangePasswordUseCase mockedChangePasswordUseCase;

  setUpAll(() {
    mockedUserProfileUseCase = MockUserProfileUseCase();
    mockedChangePasswordUseCase = MockChangePasswordUseCase();
  });

  ProfileSettingBloc createBloc() => ProfileSettingBloc(mockedUserProfileUseCase, mockedChangePasswordUseCase);

  blocTest<ProfileSettingBloc, ProfileSettingState>(
    'OnTapProfileSettingEvent to update users name and last name successful',
    setUp: () {
      when(() => mockedUserProfileUseCase.call(mockedDoubleParamsEntity)).thenAnswer((_) async => const Right(Success()));
    },
    build: createBloc,
    act: (bloc) => bloc.add(OnTapProfileSettingEvent(lastName: mockedDoubleParamsEntity.lastName, name: mockedDoubleParamsEntity.name)),
    expect: () => [
      const ProfileSettingState.loading(),
      const ProfileSettingState.success(),
    ],
    verify: (_) {
      verify(() => mockedUserProfileUseCase.call(mockedDoubleParamsEntity)).called(1);
      verifyNoMoreInteractions(mockedUserProfileUseCase);
    },
  );
  blocTest<ProfileSettingBloc, ProfileSettingState>(
    'OnTapProfileSettingEvent to update users name and last name failure',
    setUp: () {
      when(() => mockedUserProfileUseCase.call(mockedDoubleParamsEntity))
          .thenAnswer((_) async => const Left(Failure(Errors.somethingWentWrong)));
    },
    build: createBloc,
    act: (bloc) => bloc.add(OnTapProfileSettingEvent(lastName: mockedDoubleParamsEntity.lastName, name: mockedDoubleParamsEntity.name)),
    expect: () => [const ProfileSettingState.failure(Errors.somethingWentWrong)],
    verify: (_) {
      verify(() => mockedUserProfileUseCase.call(mockedDoubleParamsEntity)).called(1);
      verifyNoMoreInteractions(mockedUserProfileUseCase);
    },
  );

  blocTest<ProfileSettingBloc, ProfileSettingState>(
    'OnTapChangePassword to change users password successful',
    setUp: () {
      when(() => mockedChangePasswordUseCase.send('aaa')).thenAnswer((_) async => const Right(Success()));
    },
    build: createBloc,
    act: (bloc) => bloc.add(const OnTapChangePasswordEvent(currentEmail: 'aaa')),
    expect: () => [
      const ProfileSettingState.loading(),
      const ProfileSettingState.success(),
    ],
    verify: (_) {
      verify(() => mockedChangePasswordUseCase.send('aaa')).called(1);
      verifyNoMoreInteractions(mockedChangePasswordUseCase);
    },
  );

  blocTest<ProfileSettingBloc, ProfileSettingState>(
    'OnTapChangePassword to change users password failure',
    setUp: () {
      when(() => mockedChangePasswordUseCase.send('aaa')).thenAnswer((_) async => const Left(Failure(Errors.somethingWentWrong)));
    },
    build: createBloc,
    act: (bloc) => bloc.add(const OnTapChangePasswordEvent(currentEmail: 'aaa')),
    expect: () => [const ProfileSettingState.failure(Errors.somethingWentWrong)],
    verify: (_) {
      verify(() => mockedChangePasswordUseCase.send('aaa')).called(1);
      verifyNoMoreInteractions(mockedChangePasswordUseCase);
    },
  );
}
