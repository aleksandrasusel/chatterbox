import 'package:chatterbox/data/repositories/community_repository/chat_data_repository_impl.dart';
import 'package:chatterbox/domain/data_source/authentication_remote_source/authentication_data_source.dart';
import 'package:chatterbox/domain/data_source/chat_data_source/chat_data_source.dart';
import 'package:chatterbox/domain/data_source/community_data_source/community_data_source.dart';
import 'package:chatterbox/domain/entities/chat/chat_tile_entity.dart';
import 'package:chatterbox/domain/entities/chat/message_entity.dart';
import 'package:chatterbox/domain/repositories/chat_data_repository.dart';
import 'package:chatterbox/domain/utils/exception.dart';
import 'package:chatterbox/domain/utils/success.dart';
import 'package:chatterbox/presentation/utils/enums/errors.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

import '../../../mocks.dart';
import '../../../mocks_data.dart';

void main() {
  late ChatDataRepository repository;
  late ChatDataSource mockedChatDataSource;
  late CommunityDataSource mockedCommunityDataSource;
  late AuthenticationDataSource mockedAuthenticationDataSource;

  setUpAll(() {
    mockedChatDataSource = MockChatDataSource();
    mockedCommunityDataSource = MockCommunityDataSource();
    mockedAuthenticationDataSource = MockAuthenticationDataSource();
    repository = ChatDataRepositoryImpl(mockedChatDataSource, mockedCommunityDataSource, mockedAuthenticationDataSource);
  });

  test('GetChatMessages function to get messages with friend successful', () async {
    when(() => mockedChatDataSource.getMessages('aaa')).thenAnswer((_) => Stream.value(mockedMessageDtoList));
    final expectedMessageEntityList = mockedMessageDtoList.map((dto) => MessageEntity.fromDto(dto)).toList();
    final result = repository.getChatMessages('aaa');
    await expectLater(result, emitsInOrder([expectedMessageEntityList, emitsDone]));
    verify(() => mockedChatDataSource.getMessages('aaa'));
    verifyNoMoreInteractions(mockedChatDataSource);
  });

  test('SendMessage function to send messages successful', () async {
    when(() => mockedChatDataSource.sendMessage('aaa', 'aaa')).thenAnswer((_) async => const Success());
    final result = await repository.sendMessage('aaa', 'aaa');
    Success? success;
    result.fold((l) => null, (r) => success = r);
    expect(success, const Success());
    verify(() => mockedChatDataSource.sendMessage('aaa', 'aaa'));
    verifyNoMoreInteractions(mockedChatDataSource);
  });

  test('SendMessage function to send messages failure', () async {
    when(() => mockedChatDataSource.sendMessage('aaa', 'aaa')).thenThrow(ApiException(Errors.somethingWentWrong));
    final result = await repository.sendMessage('aaa', 'aaa');
    Errors? error;
    result.fold((l) => error = l.appError, (r) => null);
    expect(error, Errors.somethingWentWrong);
    verify(() => mockedChatDataSource.sendMessage('aaa', 'aaa'));
    verifyNoMoreInteractions(mockedChatDataSource);
  });

  test('GetChatList function to get all chats in list successful', () async {
    when(() => mockedAuthenticationDataSource.getCurrentUserId()).thenReturn('userId');
    when(() => mockedChatDataSource.getChatsList()).thenAnswer((_) async => mockedConversationDetailsDtoList);
    when(() => mockedCommunityDataSource.getFriend(mockedFriedDto1.userId)).thenAnswer((_) async => mockedFriedDto1);
    when(() => mockedCommunityDataSource.getFriend(mockedFriedDto2.userId)).thenAnswer((_) async => mockedFriedDto2);
    when(() => mockedCommunityDataSource.getFriend(mockedFriedDto3.userId)).thenAnswer((_) async => mockedFriedDto3);

    final result = await repository.getChatsList();

    List<ChatTileEntity>? chatTileList;
    result.fold(
      (l) => null,
      (r) => chatTileList = r,
    );

    expect(chatTileList, mockedChatTileEntityListResult);

    verify(() => mockedAuthenticationDataSource.getCurrentUserId());
    verify(() => mockedChatDataSource.getChatsList());
    verify(() => mockedCommunityDataSource.getFriend(any()));
    verifyNoMoreInteractions(mockedChatDataSource);
    verifyNoMoreInteractions(mockedCommunityDataSource);
    verifyNoMoreInteractions(mockedAuthenticationDataSource);
  });
}
