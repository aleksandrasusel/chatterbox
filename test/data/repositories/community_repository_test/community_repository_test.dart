import 'package:chatterbox/data/repositories/community_repository/community_repository_impl.dart';
import 'package:chatterbox/domain/data_source/community_data_source/community_data_source.dart';
import 'package:chatterbox/domain/entities/community_entity/friend_entity.dart';
import 'package:chatterbox/domain/repositories/community_repository/community_repository.dart';
import 'package:chatterbox/domain/utils/exception.dart';
import 'package:chatterbox/domain/utils/success.dart';
import 'package:chatterbox/presentation/utils/enums/errors.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

import '../../../mocks.dart';
import '../../../mocks_data.dart';

void main() {
  late CommunityDataSource mockedCommunityDataSource;
  late CommunityRepository repository;

  setUpAll(() {
    mockedCommunityDataSource = MockCommunityDataSource();
    repository = CommunityRepositoryImpl(mockedCommunityDataSource);
  });

  test('GetUsers function to getting all users successful', () async {
    when(() => mockedCommunityDataSource.getUsers()).thenAnswer((_) async => mockedFriendDtoList);

    final result = await repository.getUsers();

    List<FriendEntity>? actualFriends;
    result.fold(
      (l) => null,
      (r) => actualFriends = r,
    );

    expect(actualFriends, mockedFriendDtoList.map((e) => FriendEntity.fromDto(e)).toList());

    verify(() => mockedCommunityDataSource.getUsers());
    verifyNoMoreInteractions(mockedCommunityDataSource);
  });

  test('GetUsers function to getting all users failure', () async {
    when(() => mockedCommunityDataSource.getUsers()).thenThrow(ApiException(Errors.somethingWentWrong));
    final result = await repository.getUsers();

    Errors? failure;
    result.fold((l) => failure = l.appError, (r) => null);

    expect(failure, Errors.somethingWentWrong);
    verify(() => mockedCommunityDataSource.getUsers());
    verifyNoMoreInteractions(mockedCommunityDataSource);
  });

  test('AddFriend function to adding friend to FriendList successful', () async {
    when(() => mockedCommunityDataSource.addFriend(mockedFriedDto)).thenAnswer((_) async => const Success());
    final result = await repository.addFriend(mockedFriedEntity);
    Success? success;
    result.fold((l) => null, (r) => success = r);
    expect(success, const Success());
    verify(() => mockedCommunityDataSource.addFriend(mockedFriedDto));
    verifyNoMoreInteractions(mockedCommunityDataSource);
  });

  test('AddFriend function to adding friend to FriendList failure', () async {
    when(() => mockedCommunityDataSource.addFriend(mockedFriedDto)).thenThrow(ApiException(Errors.somethingWentWrong));
    final result = await repository.addFriend(mockedFriedEntity);
    Errors? error;
    result.fold((l) => error = l.appError, (r) => null);
    expect(error, Errors.somethingWentWrong);
    verify(() => mockedCommunityDataSource.addFriend(mockedFriedDto));
    verifyNoMoreInteractions(mockedCommunityDataSource);
  });

  test('GetFriendList function to fetch FriendList successful', () async {
    when(() => mockedCommunityDataSource.getFriendsList()).thenAnswer((_) async => mockedFriendDtoList);
    final result = await repository.getFriendsList();
    List<FriendEntity>? actualFriends;
    result.fold((l) => null, (r) => actualFriends = r);
    expect(actualFriends, mockedFriendDtoList.map((e) => FriendEntity.fromDto(e)).toList());
    verify(() => mockedCommunityDataSource.getFriendsList());
    verifyNoMoreInteractions(mockedCommunityDataSource);
  });

  test('GetFriendList function to fetch FriendList failure', () async {
    when(() => mockedCommunityDataSource.getFriendsList()).thenThrow(ApiException(Errors.somethingWentWrong));
    final result = await repository.getFriendsList();
    Errors? error;
    result.fold((l) => error = l.appError, (r) => null);
    expect(error, Errors.somethingWentWrong);
    verify(() => mockedCommunityDataSource.getFriendsList());
    verifyNoMoreInteractions(mockedCommunityDataSource);
  });

  test('DeleteFriendList function to delete friend and fetch new FriendList successful', () async {
    when(() => mockedCommunityDataSource.deleteFriend(1)).thenAnswer((_) async => mockedFriendDtoList);
    final result = await repository.deleteFriend(1);
    List<FriendEntity>? actualFriends;
    result.fold((l) => null, (r) => actualFriends = r);
    expect(actualFriends, mockedFriendDtoList.map((e) => FriendEntity.fromDto(e)).toList());
    verify(() => mockedCommunityDataSource.deleteFriend(1));
    verifyNoMoreInteractions(mockedCommunityDataSource);
  });

  test('DeleteFriendList function to delete friend and fetch new FriendList failure', () async {
    when(() => mockedCommunityDataSource.deleteFriend(1)).thenThrow(ApiException(Errors.somethingWentWrong));
    final result = await repository.deleteFriend(1);
    Errors? error;
    result.fold((l) => error = l.appError, (r) => null);
    expect(error, Errors.somethingWentWrong);
    verify(() => mockedCommunityDataSource.deleteFriend(1));
    verifyNoMoreInteractions(mockedCommunityDataSource);
  });
}
