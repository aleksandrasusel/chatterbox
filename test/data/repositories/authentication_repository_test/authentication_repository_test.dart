import 'package:chatterbox/data/repositories/authentication_repository/authentication_repository_impl.dart';
import 'package:chatterbox/domain/data_source/authentication_remote_source/authentication_data_source.dart';
import 'package:chatterbox/domain/entities/authentication/user_profile_entity.dart';
import 'package:chatterbox/domain/repositories/authentication_repository/authentication_repository.dart';
import 'package:chatterbox/domain/utils/exception.dart';
import 'package:chatterbox/domain/utils/success.dart';
import 'package:chatterbox/presentation/utils/enums/errors.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

import '../../../mocks.dart';
import '../../../mocks_data.dart';

void main() {
  late AuthenticationDataSource mockedAuthenticationDataSource;
  late User mockedUser;
  late UserCredential mockedUserCredential;
  late AuthenticationRepository repository;

  setUpAll(() {
    mockedAuthenticationDataSource = MockAuthenticationDataSource();
    mockedUser = MockUser(uid: '1');
    mockedUserCredential = MockUserCredential();
    repository = AuthenticationRepositoryImpl(mockedAuthenticationDataSource);
  });

  test('createUser function to create a user with success', () async {
    when(() => mockedAuthenticationDataSource.createUser(mockedCreateUserDto)).thenAnswer((_) async => mockedUserCredential);
    final result = await repository.createUser(mockedCreateUserEntity);
    UserCredential? actualCredential;
    result.fold(
      (l) => null,
      (r) => actualCredential = r, // Capture the right side value
    );
    expect(actualCredential, mockedUserCredential);
    verify(() => mockedAuthenticationDataSource.createUser(mockedCreateUserDto));
    verifyNoMoreInteractions(mockedAuthenticationDataSource);
  });

  test('createUser function to create a user with failure', () async {
    when(() => mockedAuthenticationDataSource.createUser(mockedCreateUserDto)).thenThrow(ApiException(Errors.somethingWentWrong));

    final result = await repository.createUser(mockedCreateUserEntity);
    Errors? failure;
    result.fold(
      (l) => failure = l.appError,
      (r) => r,
    );

    expect(failure, Errors.somethingWentWrong);
    verify(() => mockedAuthenticationDataSource.createUser(mockedCreateUserDto));
    verifyNoMoreInteractions(mockedAuthenticationDataSource);
  });

  test('createUser function to login with success', () async {
    when(() => mockedAuthenticationDataSource.login(mockedLoginDto)).thenAnswer((_) async => mockedUser);
    final result = await repository.login(mockedLoginEntity);
    User? actualUser;
    result.fold(
      (l) => null,
      (r) => actualUser = r, // Capture the right side value
    );
    expect(actualUser, mockedUser);
    verify(() => mockedAuthenticationDataSource.login(mockedLoginDto));
    verifyNoMoreInteractions(mockedAuthenticationDataSource);
  });

  test('createUser function to login with failure', () async {
    when(() => mockedAuthenticationDataSource.login(mockedLoginDto)).thenThrow(ApiException(Errors.somethingWentWrong));
    final result = await repository.login(mockedLoginEntity);
    Errors? failure;
    result.fold(
      (l) => failure = l.appError,
      (r) => r, // Capture the right side value
    );
    expect(failure, Errors.somethingWentWrong);
    verify(() => mockedAuthenticationDataSource.login(mockedLoginDto));
    verifyNoMoreInteractions(mockedAuthenticationDataSource);
  });

  test('createUser function to change password with success', () async {
    when(() => mockedAuthenticationDataSource.changePassword("a@gmail.com")).thenAnswer((_) async => const Success());
    final result = await repository.changePassword("a@gmail.com");
    Success? success;
    result.fold(
      (l) => null,
      (r) => success = r,
    );
    expect(success, const Success());
    verify(() => mockedAuthenticationDataSource.changePassword("a@gmail.com"));
    verifyNoMoreInteractions(mockedAuthenticationDataSource);
  });

  test('createUser function to change password with failure', () async {
    when(() => mockedAuthenticationDataSource.changePassword("a@gmail.com")).thenThrow(ApiException(Errors.somethingWentWrong));
    final result = await repository.changePassword("a@gmail.com");
    Errors? failure;
    result.fold(
      (l) => failure = l.appError,
      (r) => r, // Capture the right side value
    );
    expect(failure, Errors.somethingWentWrong);
    verify(() => mockedAuthenticationDataSource.changePassword("a@gmail.com"));
    verifyNoMoreInteractions(mockedAuthenticationDataSource);
  });

  test('createUser function to update user profile with success', () async {
    when(() => mockedAuthenticationDataSource.updateUserProfile(mockedUserProfileDto)).thenAnswer((_) async => const Success());
    final result = await repository.updateUserProfile(mockedUserProfileEntity);
    Success? success;
    result.fold(
      (l) => null,
      (r) => success = r,
    );
    expect(success, const Success());
    verify(() => mockedAuthenticationDataSource.updateUserProfile(mockedUserProfileDto));
    verifyNoMoreInteractions(mockedAuthenticationDataSource);
  });

  test('createUser function to update user profile with failure', () async {
    when(() => mockedAuthenticationDataSource.updateUserProfile(mockedUserProfileDto))
        .thenThrow(ApiException(Errors.somethingWentWrong));
    final result = await repository.updateUserProfile(mockedUserProfileEntity);
    Errors? failure;
    result.fold(
      (l) => failure = l.appError,
      (r) => null,
    );
    expect(failure, Errors.somethingWentWrong);
    verify(() => mockedAuthenticationDataSource.updateUserProfile(mockedUserProfileDto));
    verifyNoMoreInteractions(mockedAuthenticationDataSource);
  });

  test('createUser function to get user profile with success', () async {
    when(() => mockedAuthenticationDataSource.getUserProfile()).thenAnswer((_) async => mockedUserProfileDto);
    final result = await repository.getUserProfile();
    UserProfileEntity? entity;
    result.fold(
      (l) => null,
      (r) => entity = r,
    );
    expect(entity, UserProfileEntity.fromDto(mockedUserProfileDto));
    verify(() => mockedAuthenticationDataSource.getUserProfile());
    verifyNoMoreInteractions(mockedAuthenticationDataSource);
  });

  test('createUser function to get user profile with failure', () async {
    when(() => mockedAuthenticationDataSource.getUserProfile()).thenThrow(ApiException(Errors.somethingWentWrong));
    final result = await repository.getUserProfile();
    Errors? failure;
    result.fold(
      (l) => failure = l.appError,
      (r) => null,
    );
    expect(failure, Errors.somethingWentWrong);
    verify(() => mockedAuthenticationDataSource.getUserProfile());
    verifyNoMoreInteractions(mockedAuthenticationDataSource);
  });

  test('createUser function to get current user  with success', () {
    when(() => mockedAuthenticationDataSource.getCurrentUserId()).thenAnswer((_) => 'aa');
    final result = repository.getCurrentUserId();

    expect(result, 'aa');
    verify(() => mockedAuthenticationDataSource.getCurrentUserId());
    verifyNoMoreInteractions(mockedAuthenticationDataSource);
  });

  test('createUser function to get current user  with failure', () {
    when(() => mockedAuthenticationDataSource.getCurrentUserId()).thenThrow(ApiException(Errors.somethingWentWrong));
    expect(() => repository.getCurrentUserId(), throwsA(isA<ApiException>()));
    verify(() => mockedAuthenticationDataSource.getCurrentUserId());
    verifyNoMoreInteractions(mockedAuthenticationDataSource);
  });

  test('createUser function to sign out with success', () async {
    when(() => mockedAuthenticationDataSource.signOut()).thenAnswer((_) async => const Success());
    final result = await repository.signOut();
    Success? success;
    result.fold(
      (l) => null,
      (r) => success = r,
    );
    expect(success, const Success());
    verify(() => mockedAuthenticationDataSource.signOut());
    verifyNoMoreInteractions(mockedAuthenticationDataSource);
  });

  test('createUser function to sign out with failure', () async {
    when(() => mockedAuthenticationDataSource.signOut()).thenThrow(ApiException(Errors.somethingWentWrong));
    final result = await repository.signOut();
    Errors? failure;
    result.fold(
      (l) => failure = l.appError,
      (r) => null,
    );
    expect(failure, Errors.somethingWentWrong);
    verify(() => mockedAuthenticationDataSource.signOut());
    verifyNoMoreInteractions(mockedAuthenticationDataSource);
  });

  //   /// 1. setup
  //   when(() => mockedGetUserProfileUseCase()).thenAnswer((_) async => const Right(mockedUserProfileEntity));
  //
  //   /// 2. act
  //
  //   final blocTest = createBloc()..add(const OnInitializeAppEvent());
  //   await pumpEventQueue();
  //
  //   /// 3.Expect
  //   expect(blocTest.state, const AppState.initial('Janusz'));
  //   emitsInOrder([
  //     const AppState.loading(),
  //     const AppState.initial(''),
  //   ]);
  //
  //   verify(() => mockedGetUserProfileUseCase());
  //   verifyNoMoreInteractions(mockedGetUserProfileUseCase);
  // });
}
