import 'package:chatterbox/data/data_source/chat_data_source_impl/chat_data_source_impl.dart';
import 'package:chatterbox/data/dto/chat/conversation_details_dto.dart';
import 'package:chatterbox/data/dto/chat/message_dto.dart';
import 'package:chatterbox/data/utils/chat_id_converter.dart';
import 'package:chatterbox/domain/data_source/chat_data_source/chat_data_source.dart';
import 'package:chatterbox/domain/utils/success.dart';
import 'package:fake_cloud_firestore/fake_cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_test/flutter_test.dart';

import '../../mocks.dart';
import '../../mocks_data.dart';

void main() {
  late FakeFirebaseFirestore fakeFirebaseFirestore;
  late MockFirebaseAuth mockFirebaseAuth;
  late User mockedUser;
  late ChatDataSource chatDataSource;

  setUp(() {
    fakeFirebaseFirestore = FakeFirebaseFirestore();
    mockedUser = MockUser(uid: 'bbb');
    mockFirebaseAuth = MockFirebaseAuth(currentUser: mockedUser);
    chatDataSource = ChatDataSourceImpl(fakeFirebaseFirestore, mockFirebaseAuth);
  });

  test(
    'SendMessage sends a message successful',
    () async {
      const friendId = '1';
      final mockedConversationDetails = ConversationDetailsDto(
        lastMessage: 'aaa',
        lastMessageDate: DateTime.now(),
        chatId: chatIdConverter(friendId: friendId, userId: mockedUser.uid),
        participants: [friendId, mockedUser.uid],
      );

      await fakeFirebaseFirestore.collection('usersChat').doc('ccc').set(mockedConversationDetails.toJson());
      final dto = MessageDto(content: 'Hi World', date: DateTime.now(), senderId: mockedUser.uid);
      final result = await chatDataSource.sendMessage(dto.content, friendId);
      expect(result, const Success());

      final messagesDoc = await fakeFirebaseFirestore.collection('usersChat').doc('ccc').collection('messages').get();
      final resultMessage = MessageDto.fromJson(messagesDoc.docs.first.data());

      expect(resultMessage.content, dto.content);
      expect(resultMessage.senderId, dto.senderId);
    },
  );

  test(
    'GetMessages returns messages successful in a stream',
    () async {
      const friendId = '1';
      final userDoc = fakeFirebaseFirestore.collection('usersChat').doc('ccc');
      final messageDoc = fakeFirebaseFirestore.collection('usersChat').doc('ccc').collection('messages');
      final mockedConversationDetails = ConversationDetailsDto(
        lastMessage: 'aaa',
        lastMessageDate: DateTime.now(),
        chatId: chatIdConverter(friendId: friendId, userId: mockedUser.uid),
        participants: [friendId, mockedUser.uid],
      );

      await userDoc.set(mockedConversationDetails.toJson());

      for (final message in mockedMessageDtoList) {
        await messageDoc.add(message.toJson());
      }
      final result = chatDataSource.getMessages(friendId);
      await expectLater(await result.first, mockedMessageDtoList);
    },
  );

  test(
    'GetMessages returns a empty list in a stream',
    () async {
      const friendId = '1';
      final mockedMessageDtoList = [];
      final result = chatDataSource.getMessages(friendId);
      await expectLater(await result.first, mockedMessageDtoList);
    },
  );

  test(
    'GetChatList returns a chat list successful',
    () async {
      const friendId = '1';
      const friendId1 = '2';
      final mockedConversationDetailsList = [
        ConversationDetailsDto(
          lastMessage: 'aaa',
          lastMessageDate: DateTime.now(),
          chatId: chatIdConverter(friendId: friendId, userId: mockedUser.uid),
          participants: [friendId, mockedUser.uid],
        ),
        ConversationDetailsDto(
          lastMessage: 'bbb',
          lastMessageDate: DateTime.now(),
          chatId: chatIdConverter(friendId: friendId1, userId: mockedUser.uid),
          participants: [friendId1, mockedUser.uid],
        ),
      ];
      for (final conversationDetailsDto in mockedConversationDetailsList) {
        await fakeFirebaseFirestore.collection('usersChat').add(conversationDetailsDto.toJson());
      }

      final result = await chatDataSource.getChatsList();
      expect(result, mockedConversationDetailsList);
    },
  );

  test(
    'GetChatList returns a empty chat list',
    () async {
      final mockedConversationDetailsList = [];
      final result = await chatDataSource.getChatsList();
      expect(result, mockedConversationDetailsList);
    },
  );
}
