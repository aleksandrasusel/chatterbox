import 'package:chatterbox/data/data_source/authentication_remote_source/authentication_data_source_impl.dart';
import 'package:chatterbox/data/dto/authentication/user_profile_dto.dart';
import 'package:chatterbox/domain/data_source/authentication_remote_source/authentication_data_source.dart';
import 'package:chatterbox/domain/utils/exception.dart';
import 'package:chatterbox/domain/utils/success.dart';
import 'package:chatterbox/presentation/utils/enums/errors.dart';
import 'package:fake_cloud_firestore/fake_cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

import '../../mocks.dart';
import '../../mocks_data.dart';

void main() {
  late FakeFirebaseFirestore fakeFirebaseFirestore;
  late MockFirebaseAuth mockFirebaseAuth;
  late AuthenticationDataSource authenticationDataSource;
  late UserCredential mockUserCredential;
  late UserCredential mockUserCredentialNoUser;
  late User mockedUser;
  late FirebaseException mockFirebaseException;
  setUp(() {
    fakeFirebaseFirestore = FakeFirebaseFirestore();
    mockedUser = MockUser(uid: '111');
    mockFirebaseAuth = MockFirebaseAuth(currentUser: mockedUser);
    mockUserCredential = MockUserCredential(user: mockedUser);
    mockUserCredentialNoUser = MockUserCredential();
    authenticationDataSource = AuthenticationRemoteSourceImpl(fakeFirebaseFirestore, mockFirebaseAuth);
    mockFirebaseException = MockFirebaseException();
  });

  test(
    'CreateUser creates user successful',
    () async {
      final newUserDto = UserProfileDto.newUser(mockedCreateUserDto.email, '111');
      final collection = fakeFirebaseFirestore.collection('users');

      when(
        () => mockFirebaseAuth.createUserWithEmailAndPassword(
          email: mockedCreateUserDto.email,
          password: mockedCreateUserDto.password,
        ),
      ).thenAnswer((_) async => mockUserCredential);

      final result = await authenticationDataSource.createUser(mockedCreateUserDto);
      final userDoc = await collection.doc(mockUserCredential.user?.uid).get();

      expect(userDoc.data(), newUserDto.toJson());
      expect(result, mockUserCredential);

      verify(
        () => mockFirebaseAuth.createUserWithEmailAndPassword(
          email: mockedCreateUserDto.email,
          password: mockedCreateUserDto.password,
        ),
      );
      verifyNoMoreInteractions(mockFirebaseAuth);
    },
  );

  test(
    "Login to login user ",
    () async {
      when(
        () => mockFirebaseAuth.signInWithEmailAndPassword(
          email: mockedLoginDto.email,
          password: mockedLoginDto.password,
        ),
      ).thenAnswer((_) async => mockUserCredential);

      final result = await authenticationDataSource.login(mockedLoginDto);

      expect(result, mockUserCredential.user);

      verify(() => mockFirebaseAuth.signInWithEmailAndPassword(email: mockedLoginDto.email, password: mockedLoginDto.password));
      verifyNoMoreInteractions(mockFirebaseAuth);
    },
  );

  test(
    "Login has user null",
    () async {
      when(
        () => mockFirebaseAuth.signInWithEmailAndPassword(email: mockedLoginDto.email, password: mockedLoginDto.password),
      ).thenAnswer((_) async => mockUserCredentialNoUser);

      await expectLater(
        authenticationDataSource.login(mockedLoginDto),
        throwsA(isA<ApiException>().having((e) => e.error, 'user not found', Errors.userIsNotFound)),
      );

      verify(() => mockFirebaseAuth.signInWithEmailAndPassword(email: mockedLoginDto.email, password: mockedLoginDto.password));
    },
  );

  test(
    "Login throws FirebaseException",
    () async {
      when(() => mockFirebaseAuth.signInWithEmailAndPassword(email: mockedLoginDto.email, password: mockedLoginDto.password))
          .thenThrow(mockFirebaseException);

      await expectLater(
        authenticationDataSource.login(mockedLoginDto),
        throwsA(isA<ApiException>().having((e) => e.error, 'Unknown Error', Errors.unknownError)),
      );

      verify(() => mockFirebaseAuth.signInWithEmailAndPassword(email: mockedLoginDto.email, password: mockedLoginDto.password));
      verifyNoMoreInteractions(mockFirebaseAuth);
    },
  );

  test(
    "Login throws ApiException on catch",
    () async {
      when(() => mockFirebaseAuth.signInWithEmailAndPassword(email: mockedLoginDto.email, password: mockedLoginDto.password))
          .thenThrow(Exception());

      await expectLater(
        authenticationDataSource.login(mockedLoginDto),
        throwsA(isA<ApiException>().having((e) => e.error, 'Unknown Error', Errors.unknownError)),
      );

      verify(() => mockFirebaseAuth.signInWithEmailAndPassword(email: mockedLoginDto.email, password: mockedLoginDto.password));
      verifyNoMoreInteractions(mockFirebaseAuth);
    },
  );

  test(
    "ChangePassword changes user's password successful",
    () async {
      when(() => mockFirebaseAuth.sendPasswordResetEmail(email: mockedLoginDto.email)).thenAnswer((_) async => const Success());
      final result = await authenticationDataSource.changePassword(mockedLoginDto.email);
      expect(result, const Success());
      verify(() => mockFirebaseAuth.sendPasswordResetEmail(email: mockedLoginDto.email));
      verifyNoMoreInteractions(mockFirebaseAuth);
    },
  );

  test(
    "ChangePassword changes user's password failure",
    () async {
      when(() => mockFirebaseAuth.sendPasswordResetEmail(email: mockedLoginDto.email)).thenThrow((_) async => Exception());

      await expectLater(
        authenticationDataSource.changePassword(mockedLoginDto.email),
        throwsA(
          isA<ApiException>().having((e) => e.error, 'user not found', Errors.invalidEmail),
        ),
      );
      verify(() => mockFirebaseAuth.sendPasswordResetEmail(email: mockedLoginDto.email));
      verifyNoMoreInteractions(mockFirebaseAuth);
    },
  );

  test(
    "UpdateUserProfile updates user's profile",
    () async {
      final dto = UserProfileDto(
        email: mockedUserProfileDto.email,
        name: mockedUserProfileDto.name,
        lastName: mockedUserProfileDto.lastName,
        avatar: mockedUserProfileDto.avatar,
        userId: mockedUser.uid,
        friends: mockedUserProfileDto.friends,
      );

      final collection = fakeFirebaseFirestore.collection('users');
      final result = await authenticationDataSource.updateUserProfile(dto);
      final userDoc = await collection.doc(dto.userId).get();

      expect(userDoc.data(), dto.toJson());
      expect(result, const Success());
    },
  );

  test(
    "GetCurrentUserId gets user id successful",
    () {
      final result = authenticationDataSource.getCurrentUserId();
      expect(result, mockedUser.uid);
    },
  );

  test(
    "GetUsersProfile gets user's profile successful",
    () async {
      final dto = UserProfileDto(
        email: mockedUserProfileDto.email,
        name: mockedUserProfileDto.name,
        lastName: mockedUserProfileDto.lastName,
        avatar: mockedUserProfileDto.avatar,
        userId: mockedUser.uid,
        friends: mockedUserProfileDto.friends,
      );
      final collection = fakeFirebaseFirestore.collection('users');
      await collection.doc(dto.userId).set(dto.toJson());
      final result = await authenticationDataSource.getUserProfile();
      expect(result, dto);
    },
  );

  test(
    "GetUsersProfile gets user's profile when document does not exist",
    () async {
      await expectLater(
        authenticationDataSource.getUserProfile(),
        throwsA(
          isA<ApiException>().having((e) => e.error, 'something went wrong', Errors.somethingWentWrong),
        ),
      );
    },
  );

  test(
    "SignOut to logout the user successful",
    () async {
      when(() => mockFirebaseAuth.signOut()).thenAnswer((_) async => const Success());
      final result = await authenticationDataSource.signOut();
      expect(result, const Success());

      verify(() => mockFirebaseAuth.signOut());
      verifyNoMoreInteractions(mockFirebaseAuth);
    },
  );

  test(
    "SignOut to logout the user failure",
    () async {
      when(() => mockFirebaseAuth.signOut()).thenThrow(Exception());

      await expectLater(
        authenticationDataSource.signOut(),
        throwsA(
          isA<ApiException>().having((e) => e.error, 'something went wrong', Errors.somethingWentWrong),
        ),
      );
      verify(() => mockFirebaseAuth.signOut());
      verifyNoMoreInteractions(mockFirebaseAuth);
    },
  );
}
