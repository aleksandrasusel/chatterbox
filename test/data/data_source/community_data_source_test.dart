import 'package:chatterbox/data/data_source/community_data_source/community_data_source_impl.dart';
import 'package:chatterbox/data/dto/user_community/friend_dto.dart';
import 'package:chatterbox/domain/data_source/community_data_source/community_data_source.dart';
import 'package:chatterbox/domain/utils/exception.dart';
import 'package:chatterbox/domain/utils/success.dart';
import 'package:chatterbox/presentation/utils/enums/errors.dart';
import 'package:fake_cloud_firestore/fake_cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_test/flutter_test.dart';

import '../../mocks.dart';
import '../../mocks_data.dart';

void main() {
  late FakeFirebaseFirestore fakeFirebaseFirestore;
  late MockFirebaseAuth mockFirebaseAuth;
  late User mockedUser;
  late CommunityDataSource communityDataSource;

  setUp(() {
    fakeFirebaseFirestore = FakeFirebaseFirestore();
    mockedUser = MockUser(uid: '111');
    mockFirebaseAuth = MockFirebaseAuth(currentUser: mockedUser);
    communityDataSource = CommunityDataSourceImpl(fakeFirebaseFirestore, mockFirebaseAuth);
  });

  test(
    'GetUsers gets users list successful',
    () async {
      for (final friend in mockedFriendDtoList) {
        await fakeFirebaseFirestore.collection('users').doc(friend.userId).set(friend.toJson());
      }

      final result = await communityDataSource.getUsers();

      expect(result, mockedFriendDtoList);
    },
  );

  test(
    'GetFriendsList gets friends list successful',
    () async {
      await fakeFirebaseFirestore.collection('users').doc(mockedUser.uid).set(mockedUserProfileDtoWithFriends.toJson());
      final result = await communityDataSource.getFriendsList();
      expect(result, mockedUserProfileDtoWithFriends.friends);
    },
  );
  test(
    "GetFriendsList gets friends list when user's doc doesnt exist",
    () async {
      await fakeFirebaseFirestore.collection('users').doc(null).set(mockedUserProfileDto.toJson());
      await expectLater(
        communityDataSource.getFriendsList(),
        throwsA(isA<ApiException>().having((e) => e.error, 'something went wrong', Errors.somethingWentWrong)),
      );
    },
  );

  test(
    'AddNewFriend adds new friend to list successful',
    () async {
      await fakeFirebaseFirestore.collection('users').doc(mockedUser.uid).set(mockedUserProfileDtoWithFriends.toJson());
      final result = await communityDataSource.addFriend(mockedFriedDto);
      expect(result, const Success());
    },
  );

  test(
    'AddNewFriend adds new friend to list failure',
    () async {
      await expectLater(
        communityDataSource.addFriend(mockedFriedDto),
        throwsA(isA<ApiException>().having((e) => e.error, "something went wrong", Errors.somethingWentWrong)),
      );
    },
  );

  test(
    'DeleteFriend deletes friend successful',
    () async {
      await fakeFirebaseFirestore.collection('users').doc(mockedUser.uid).set(mockedUserProfileDtoWithFriends.toJson());
      final result = await communityDataSource.deleteFriend(1);
      expect(result, mockedUserProfileDtoWithFriends1.friends);
    },
  );

  test(
    'DeleteFriend deletes friend failure',
    () async {
      await fakeFirebaseFirestore.collection('users').doc(mockedUser.uid).set(mockedUserProfileDtoWithFriends.toJson());
      await expectLater(
        communityDataSource.deleteFriend(4),
        throwsA(isA<ApiException>().having((e) => e.error, "something went wrong", Errors.somethingWentWrong)),
      );
    },
  );

  test(
    'GetFriend gets a friend successful',
    () async {
      await fakeFirebaseFirestore.collection('users').doc(mockedUser.uid).set(mockedUserProfileDto.toJson());
      final result = await communityDataSource.getFriend(mockedUser.uid);
      final fakeUser = FriendDto.fromJson(mockedUserProfileDto.toJson());
      expect(result, fakeUser);
    },
  );

  test(
    "GetFriend gets a friend when doc doesn't exist",
    () async {
      await expectLater(
        communityDataSource.getFriend(mockedUser.uid),
        throwsA(isA<ApiException>().having((e) => e.error, "something went wrong", Errors.somethingWentWrong)),
      );
    },
  );
}
