import 'package:chatterbox/domain/use_case/chat_use_case/send_message_use_case.dart';
import 'package:chatterbox/domain/utils/failure.dart';
import 'package:chatterbox/domain/utils/success.dart';
import 'package:chatterbox/presentation/utils/enums/errors.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

import '../../../mocks.dart';
import '../../../mocks_data.dart';

void main() {
  late MockChatDataRepository mockChatDataRepository;
  late SendMessageUseCase sendMessageUseCase;
  setUp(() {
    mockChatDataRepository = MockChatDataRepository();
    sendMessageUseCase = SendMessageUseCase(mockChatDataRepository);
  });

  test('SendMessage sends a message success', () async {
    when(() => mockChatDataRepository.sendMessage(sendMessageParams.message, sendMessageParams.friendId))
        .thenAnswer((_) async => const Right(Success()));
    Success? success;
    final result = await sendMessageUseCase.send(sendMessageParams);
    result.fold((l) => null, (r) => success = r);
    expect(success, const Success());
    verify(() => mockChatDataRepository.sendMessage(sendMessageParams.message, sendMessageParams.friendId));
    verifyNoMoreInteractions(mockChatDataRepository);
  });

  test('SendMessage sends a message failure', () async {
    when(() => mockChatDataRepository.sendMessage(sendMessageParams.message, sendMessageParams.friendId))
        .thenAnswer((_) async => const Left(Failure(Errors.somethingWentWrong)));
    Errors? error;
    final result = await sendMessageUseCase.send(sendMessageParams);
    result.fold((l) => error = l.appError, (r) => null);
    expect(error, Errors.somethingWentWrong);
    verify(() => mockChatDataRepository.sendMessage(sendMessageParams.message, sendMessageParams.friendId));
    verifyNoMoreInteractions(mockChatDataRepository);
  });
}
