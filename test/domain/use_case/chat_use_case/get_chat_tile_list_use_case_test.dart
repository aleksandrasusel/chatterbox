import 'package:chatterbox/domain/entities/chat/chat_tile_entity.dart';
import 'package:chatterbox/domain/use_case/chat_use_case/get_chat_tile_list_use_case.dart';
import 'package:chatterbox/domain/utils/failure.dart';
import 'package:chatterbox/presentation/utils/enums/errors.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

import '../../../mocks.dart';
import '../../../mocks_data.dart';

void main() {
  late MockChatDataRepository mockChatDataRepository;
  late GetChatTileListUseCase getChatTileListUseCase;
  setUp(() {
    mockChatDataRepository = MockChatDataRepository();
    getChatTileListUseCase = GetChatTileListUseCase(mockChatDataRepository);
  });

  test('GetChatTileListUseCase returns list of chats tile success', () async {
    when(() => mockChatDataRepository.getChatsList()).thenAnswer((_) async => Right(mockedChatTileEntityList));
    List<ChatTileEntity>? success;
    final result = await getChatTileListUseCase.call();
    result.fold((l) => null, (r) => success = r);
    expect(success, mockedChatTileEntityList);
    verify(() => mockChatDataRepository.getChatsList());
    verifyNoMoreInteractions(mockChatDataRepository);
  });

  test('GetChatTileListUseCase returns list of chats tile success', () async {
    when(() => mockChatDataRepository.getChatsList()).thenAnswer((_) async => const Left(Failure(Errors.unknownError)));

    final result = await getChatTileListUseCase.call();
    Errors? error;
    result.fold((l) => error = l.appError, (r) => null);
    expect(error, Errors.unknownError);
    verify(() => mockChatDataRepository.getChatsList());
    verifyNoMoreInteractions(mockChatDataRepository);
  });
}
