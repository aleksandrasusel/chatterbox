import 'package:chatterbox/domain/use_case/chat_use_case/get_messages_use_case.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

import '../../../mocks.dart';
import '../../../mocks_data.dart';

void main() {
  late MockChatDataRepository mockChatDataRepository;
  late GetMessagesUseCase getMessagesUseCase;
  setUp(() {
    mockChatDataRepository = MockChatDataRepository();
    getMessagesUseCase = GetMessagesUseCase(mockChatDataRepository);
  });

  test('GetChatTileListUseCase returns list of chats tile success', () async {
    when(() => mockChatDataRepository.getChatMessages(any())).thenAnswer((_) => Stream.value(mockedMessageEntityList));
    when(() => mockChatDataRepository.getChatMessages(any())).thenAnswer((_) => Stream.fromIterable([mockedMessageEntityList]));
    final stream = getMessagesUseCase('aaa');
    await expectLater(stream, emitsInOrder([mockedMessageEntityList]));
    verify(() => mockChatDataRepository.getChatMessages(any()));
    verifyNoMoreInteractions(mockChatDataRepository);
  });
}
