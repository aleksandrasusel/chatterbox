import 'package:chatterbox/domain/entities/community_entity/friend_entity.dart';
import 'package:chatterbox/domain/use_case/community_use_case/get_friends_list_use_case.dart';
import 'package:chatterbox/domain/utils/failure.dart';
import 'package:chatterbox/presentation/utils/enums/errors.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

import '../../../mocks.dart';
import '../../../mocks_data.dart';

void main() {
  late MockCommunityRepository mockCommunityRepository;
  late GetFriendsListUseCase getFriendsListUseCase;
  setUp(() {
    mockCommunityRepository = MockCommunityRepository();
    getFriendsListUseCase = GetFriendsListUseCase(mockCommunityRepository);
  });

  test('GetFriendsList fetches friends list success', () async {
    when(() => mockCommunityRepository.getFriendsList()).thenAnswer((_) async => Right(mockedFriendEntityList));
    List<FriendEntity>? friendList;
    final result = await getFriendsListUseCase.call();
    result.fold((l) => null, (r) => friendList = r);
    expect(friendList, mockedFriendEntityList);
    verify(() => mockCommunityRepository.getFriendsList());
    verifyNoMoreInteractions(mockCommunityRepository);
  });
  test('GetFriendsList fetches friends list failure', () async {
    when(() => mockCommunityRepository.getFriendsList()).thenAnswer(
      (_) async => const Left(Failure(Errors.somethingWentWrong)),
    );
    Errors? error;
    final result = await getFriendsListUseCase.call();
    result.fold((l) => error = l.appError, (r) => null);
    expect(error, Errors.somethingWentWrong);
    verify(() => mockCommunityRepository.getFriendsList());
    verifyNoMoreInteractions(mockCommunityRepository);
  });
}
