import 'package:chatterbox/domain/entities/community_entity/friend_entity.dart';
import 'package:chatterbox/domain/use_case/community_use_case/delete_friend_use_case.dart';
import 'package:chatterbox/domain/utils/failure.dart';
import 'package:chatterbox/presentation/utils/enums/errors.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

import '../../../mocks.dart';
import '../../../mocks_data.dart';

void main() {
  late MockCommunityRepository mockCommunityRepository;
  late DeleteFriendUseCase deleteFriendUseCase;
  setUp(() {
    mockCommunityRepository = MockCommunityRepository();
    deleteFriendUseCase = DeleteFriendUseCase(mockCommunityRepository);
  });

  test('DeleteFriend removes a friend in friends list success', () async {
    when(() => mockCommunityRepository.deleteFriend(1)).thenAnswer((_) async => Right(mockedFriendEntityList));
    List<FriendEntity>? friendList;
    final result = await deleteFriendUseCase.call(1);
    result.fold((l) => null, (r) => friendList = r);
    expect(friendList, mockedFriendEntityList);
    verify(() => mockCommunityRepository.deleteFriend(1));
    verifyNoMoreInteractions(mockCommunityRepository);
  });
  test('DeleteFriend removes a friend in friends list failure', () async {
    when(() => mockCommunityRepository.deleteFriend(1)).thenAnswer(
      (_) async => const Left(Failure(Errors.somethingWentWrong)),
    );
    Errors? error;
    final result = await deleteFriendUseCase.call(1);
    result.fold((l) => error = l.appError, (r) => null);
    expect(error, Errors.somethingWentWrong);
    verify(() => mockCommunityRepository.deleteFriend(1));
    verifyNoMoreInteractions(mockCommunityRepository);
  });
}
