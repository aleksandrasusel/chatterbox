import 'package:chatterbox/domain/entities/community_entity/friend_entity.dart';
import 'package:chatterbox/domain/use_case/community_use_case/get_users_use_case.dart';
import 'package:chatterbox/domain/utils/failure.dart';
import 'package:chatterbox/presentation/utils/enums/errors.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

import '../../../mocks.dart';
import '../../../mocks_data.dart';

void main() {
  late MockCommunityRepository mockCommunityRepository;
  late GetUsersUseCase getUsersUseCase;
  setUp(() {
    mockCommunityRepository = MockCommunityRepository();
    getUsersUseCase = GetUsersUseCase(mockCommunityRepository);
  });

  test('GetUsersList fetches all users list success', () async {
    when(() => mockCommunityRepository.getUsers()).thenAnswer((_) async => Right(mockedFriendEntityList));
    List<FriendEntity>? friendList;
    final result = await getUsersUseCase.call();
    result.fold((l) => null, (r) => friendList = r);
    expect(friendList, mockedFriendEntityList);
    verify(() => mockCommunityRepository.getUsers());
    verifyNoMoreInteractions(mockCommunityRepository);
  });
  test('GetUsersList fetches all users list failure', () async {
    when(() => mockCommunityRepository.getUsers()).thenAnswer(
      (_) async => const Left(Failure(Errors.somethingWentWrong)),
    );
    Errors? error;
    final result = await getUsersUseCase.call();
    result.fold((l) => error = l.appError, (r) => null);
    expect(error, Errors.somethingWentWrong);
    verify(() => mockCommunityRepository.getUsers());
    verifyNoMoreInteractions(mockCommunityRepository);
  });
}
