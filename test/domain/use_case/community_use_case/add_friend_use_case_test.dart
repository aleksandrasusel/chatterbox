import 'package:chatterbox/domain/use_case/community_use_case/add_friend_use_case.dart';
import 'package:chatterbox/domain/utils/failure.dart';
import 'package:chatterbox/domain/utils/success.dart';
import 'package:chatterbox/presentation/utils/enums/errors.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

import '../../../mocks.dart';
import '../../../mocks_data.dart';

void main() {
  late MockCommunityRepository mockCommunityRepository;
  late AddFriendUseCase addFriendUseCase;
  setUp(() {
    mockCommunityRepository = MockCommunityRepository();
    addFriendUseCase = AddFriendUseCase(mockCommunityRepository);
  });

  test('AddFriend adds a friend to friends list success', () async {
    when(() => mockCommunityRepository.addFriend(mockedFriedEntity)).thenAnswer((_) async => const Right(Success()));
    Success? success;
    final result = await addFriendUseCase.send(mockedFriedEntity);
    result.fold((l) => null, (r) => success = r);
    expect(success, const Success());
    verify(() => mockCommunityRepository.addFriend(mockedFriedEntity));
    verifyNoMoreInteractions(mockCommunityRepository);
  });
  test('AddFriend adds a friend to friends list failure', () async {
    when(() => mockCommunityRepository.addFriend(mockedFriedEntity)).thenAnswer(
      (_) async => const Left(Failure(Errors.somethingWentWrong)),
    );
    Errors? error;
    final result = await addFriendUseCase.send(mockedFriedEntity);
    result.fold((l) => error = l.appError, (r) => null);
    expect(error, Errors.somethingWentWrong);
    verify(() => mockCommunityRepository.addFriend(mockedFriedEntity));
    verifyNoMoreInteractions(mockCommunityRepository);
  });
}
