import 'package:chatterbox/domain/entities/authentication/user_profile_entity.dart';
import 'package:chatterbox/domain/use_case/authentication_use_case/get_user_profile_use_case.dart';
import 'package:chatterbox/domain/utils/failure.dart';
import 'package:chatterbox/presentation/utils/enums/errors.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

import '../../../mocks.dart';
import '../../../mocks_data.dart';

void main() {
  late MockAuthenticationRepository mockAuthenticationRepository;
  late GetUserProfileUseCase getUserProfileUseCase;

  setUp(() {
    mockAuthenticationRepository = MockAuthenticationRepository();
    getUserProfileUseCase = GetUserProfileUseCase(mockAuthenticationRepository);
  });

  test('GetUserProfileUseCase returns a user profile success', () async {
    when(() => mockAuthenticationRepository.getUserProfile()).thenAnswer((_) async => const Right(mockedUserProfileEntity));

    final result = await getUserProfileUseCase.call();
    UserProfileEntity? userProfileEntity;
    result.fold((l) => null, (r) => userProfileEntity = r);
    expect(userProfileEntity, mockedUserProfileEntity);
    verify(() => mockAuthenticationRepository.getUserProfile());
    verifyNoMoreInteractions(mockAuthenticationRepository);
  });

  test('GetUserProfileUseCase returns a user profile failure', () async {
    when(() => mockAuthenticationRepository.getUserProfile()).thenAnswer((_) async => const Left(Failure(Errors.unknownError)));

    final result = await getUserProfileUseCase.call();
    Errors? error;
    result.fold((l) => error = l.appError, (r) => null);
    expect(error, Errors.unknownError);
    verify(() => mockAuthenticationRepository.getUserProfile());
    verifyNoMoreInteractions(mockAuthenticationRepository);
  });
}
