import 'package:chatterbox/domain/use_case/authentication_use_case/user_profile_use_case.dart';
import 'package:chatterbox/domain/utils/failure.dart';
import 'package:chatterbox/domain/utils/success.dart';
import 'package:chatterbox/presentation/utils/enums/errors.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

import '../../../mocks.dart';
import '../../../mocks_data.dart';

void main() {
  late MockAuthenticationRepository mockAuthenticationRepository;
  late UserProfileUseCase userProfileUseCase;

  setUp(() {
    mockAuthenticationRepository = MockAuthenticationRepository();
    userProfileUseCase = UserProfileUseCase(mockAuthenticationRepository);
    registerFallbackValue(mockedUserProfileEntity);
  });

  test('UserProfileUseCase gets and updates information about user success', () async {
    when(() => mockAuthenticationRepository.getCurrentUserId()).thenAnswer((_) => mockedUserProfileEntity.userId);
    when(() => mockAuthenticationRepository.updateUserProfile(any())).thenAnswer((_) async => const Right(Success()));
    Success? success;
    final result = await userProfileUseCase.call(mockedDoubleParamsEntity);
    result.fold((l) => null, (r) => success = r);
    expect(success, const Success());
    verify(() => mockAuthenticationRepository.updateUserProfile(any()));
    verify(() => mockAuthenticationRepository.getCurrentUserId());
    verifyNoMoreInteractions(mockAuthenticationRepository);
  });

  test('UserProfileUseCase gets and updates information about user failure', () async {
    when(() => mockAuthenticationRepository.getCurrentUserId()).thenAnswer((_) => mockedUserProfileEntity.userId);
    when(() => mockAuthenticationRepository.updateUserProfile(any())).thenAnswer((_) async => const Left(Failure(Errors.unknownError)));
    final result = await userProfileUseCase.call(mockedDoubleParamsEntity);
    Errors? error;
    result.fold((l) => error = l.appError, (r) => null);
    expect(error, Errors.unknownError);
    verify(() => mockAuthenticationRepository.updateUserProfile(any()));
    verify(() => mockAuthenticationRepository.getCurrentUserId());
    verifyNoMoreInteractions(mockAuthenticationRepository);
  });
}
