import 'package:chatterbox/domain/use_case/authentication_use_case/registration_use_case.dart';
import 'package:chatterbox/domain/utils/failure.dart';
import 'package:chatterbox/domain/utils/success.dart';
import 'package:chatterbox/presentation/utils/enums/errors.dart';
import 'package:dartz/dartz.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

import '../../../mocks.dart';
import '../../../mocks_data.dart';

void main() {
  late MockAuthenticationRepository mockAuthenticationRepository;
  late RegistrationUseCase registrationUseCase;
  late UserCredential mockedUserCredential;
  setUp(() {
    mockAuthenticationRepository = MockAuthenticationRepository();
    registrationUseCase = RegistrationUseCase(mockAuthenticationRepository);
    mockedUserCredential = MockUserCredential();
  });

  test('RegistrationUseCase creates a user success', () async {
    when(() => mockAuthenticationRepository.createUser(mockedCreateUserEntity)).thenAnswer((_) async => Right(mockedUserCredential));

    final result = await registrationUseCase.call(mockedCreateUserEntity);
    Success? success;
    result.fold((l) => null, (r) => success = r);
    expect(success, const Success());
    verify(() => mockAuthenticationRepository.createUser(mockedCreateUserEntity));
    verifyNoMoreInteractions(mockAuthenticationRepository);
  });

  test('RegistrationUseCase creates a user failure', () async {
    when(() => mockAuthenticationRepository.createUser(mockedCreateUserEntity))
        .thenAnswer((_) async => const Left(Failure(Errors.unknownError)));
    Errors? error;
    final result = await registrationUseCase.call(mockedCreateUserEntity);
    result.fold((l) => error = l.appError, (r) => null);
    expect(error, Errors.unknownError);
    verify(() => mockAuthenticationRepository.createUser(mockedCreateUserEntity));
    verifyNoMoreInteractions(mockAuthenticationRepository);
  });
}
