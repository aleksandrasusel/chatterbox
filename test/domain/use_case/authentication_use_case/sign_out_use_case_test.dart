import 'package:chatterbox/domain/use_case/authentication_use_case/sign_out_use_case.dart';
import 'package:chatterbox/domain/utils/failure.dart';
import 'package:chatterbox/domain/utils/success.dart';
import 'package:chatterbox/presentation/utils/enums/errors.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

import '../../../mocks.dart';

void main() {
  late MockAuthenticationRepository mockAuthenticationRepository;
  late SignOutUseCase signOutUseCase;
  setUp(() {
    mockAuthenticationRepository = MockAuthenticationRepository();
    signOutUseCase = SignOutUseCase(mockAuthenticationRepository);
  });

  test('SignOutUseCase logs out a user success', () async {
    when(() => mockAuthenticationRepository.signOut()).thenAnswer((_) async => const Right(Success()));
    Success? success;
    final result = await signOutUseCase.call();
    result.fold((l) => null, (r) => success = r);
    expect(success, const Success());
    verify(() => mockAuthenticationRepository.signOut());
    verifyNoMoreInteractions(mockAuthenticationRepository);
  });

  test('SignOutUseCase logs out a user success', () async {
    when(() => mockAuthenticationRepository.signOut()).thenAnswer((_) async => const Left(Failure(Errors.unknownError)));

    final result = await signOutUseCase.call();
    Errors? error;
    result.fold((l) => error = l.appError, (r) => null);
    expect(error, Errors.unknownError);
    verify(() => mockAuthenticationRepository.signOut());
    verifyNoMoreInteractions(mockAuthenticationRepository);
  });
}
