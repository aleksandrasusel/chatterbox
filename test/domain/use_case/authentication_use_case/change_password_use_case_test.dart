import 'package:chatterbox/domain/use_case/authentication_use_case/change_password_use_case.dart';
import 'package:chatterbox/domain/utils/failure.dart';
import 'package:chatterbox/domain/utils/success.dart';
import 'package:chatterbox/presentation/utils/enums/errors.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

import '../../../mocks.dart';

void main() {
  late MockAuthenticationRepository mockAuthenticationRepository;
  late ChangePasswordUseCase changePasswordUseCase;

  setUp(() {
    mockAuthenticationRepository = MockAuthenticationRepository();
    changePasswordUseCase = ChangePasswordUseCase(mockAuthenticationRepository);
  });

  test('ChangePasswordUseCase returns on right Success', () async {
    when(() => mockAuthenticationRepository.changePassword(any())).thenAnswer((_) async => const Right(Success()));

    final result = await changePasswordUseCase.send('aaa@gmail.com');
    Success? success;
    result.fold((l) => null, (r) => success = r);
    expect(success, const Success());
    verify(() => mockAuthenticationRepository.changePassword(any()));
    verifyNoMoreInteractions(mockAuthenticationRepository);
  });

  test('ChangePasswordUseCase returns on left Failure', () async {
    when(() => mockAuthenticationRepository.changePassword(any())).thenAnswer((_) async => const Left(Failure(Errors.unknownError)));

    final result = await changePasswordUseCase.send('aaa@gmail.com');
    Failure? failure;
    result.fold((l) => failure = l, (r) => null);
    expect(failure, const Failure(Errors.unknownError));
    verify(() => mockAuthenticationRepository.changePassword(any()));
    verifyNoMoreInteractions(mockAuthenticationRepository);
  });
}
