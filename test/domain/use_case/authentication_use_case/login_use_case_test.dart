import 'package:chatterbox/domain/use_case/authentication_use_case/login_use_case.dart';
import 'package:chatterbox/domain/utils/failure.dart';
import 'package:chatterbox/presentation/utils/enums/errors.dart';
import 'package:dartz/dartz.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

import '../../../mocks.dart';
import '../../../mocks_data.dart';

void main() {
  late MockAuthenticationRepository mockAuthenticationRepository;
  late LoginUseCase loginUseCase;
  late User mockedUser;
  setUp(() {
    mockAuthenticationRepository = MockAuthenticationRepository();
    loginUseCase = LoginUseCase(mockAuthenticationRepository);
    mockedUser = MockUser(uid: '1');
  });

  test('LoginUseCase logs a user success', () async {
    when(() => mockAuthenticationRepository.login(mockedLoginEntity)).thenAnswer((_) async => Right(mockedUser));

    final result = await loginUseCase.call(mockedLoginEntity);
    User? user;
    result.fold((l) => null, (r) => user = r);
    expect(user, mockedUser);
    verify(() => mockAuthenticationRepository.login(mockedLoginEntity));
    verifyNoMoreInteractions(mockAuthenticationRepository);
  });

  test('LoginUseCase logs a user failure', () async {
    when(() => mockAuthenticationRepository.login(mockedLoginEntity)).thenAnswer((_) async => const Left(Failure(Errors.unknownError)));

    final result = await loginUseCase.call(mockedLoginEntity);
    Errors? error;
    result.fold((l) => error = l.appError, (r) => null);
    expect(error, Errors.unknownError);
    verify(() => mockAuthenticationRepository.login(mockedLoginEntity));
    verifyNoMoreInteractions(mockAuthenticationRepository);
  });
}
