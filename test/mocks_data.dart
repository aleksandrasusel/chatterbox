import 'package:chatterbox/data/dto/authentication/create_user_dto.dart';
import 'package:chatterbox/data/dto/authentication/login_dto.dart';
import 'package:chatterbox/data/dto/authentication/user_profile_dto.dart';
import 'package:chatterbox/data/dto/chat/conversation_details_dto.dart';
import 'package:chatterbox/data/dto/chat/message_dto.dart';
import 'package:chatterbox/data/dto/user_community/friend_dto.dart';
import 'package:chatterbox/domain/entities/authentication/create_user_entity.dart';
import 'package:chatterbox/domain/entities/authentication/login_entity.dart';
import 'package:chatterbox/domain/entities/authentication/user_profile_entity.dart';
import 'package:chatterbox/domain/entities/chat/chat_tile_entity.dart';
import 'package:chatterbox/domain/entities/chat/conversation_details_entity.dart';
import 'package:chatterbox/domain/entities/chat/message_entity.dart';
import 'package:chatterbox/domain/entities/community_entity/double_params_entity.dart';
import 'package:chatterbox/domain/entities/community_entity/friend_entity.dart';
import 'package:chatterbox/domain/use_case/chat_use_case/send_message_use_case.dart';

const mockedUserProfileEntity = UserProfileEntity(
  email: 'email',
  name: 'Janusz',
  lastName: 'Nowak',
  avatar: 'avatar',
  userId: 'userId',
  friends: [],
);

const mockedUserProfileDto = UserProfileDto(
  email: 'email',
  name: 'Janusz',
  lastName: 'Nowak',
  avatar: 'avatar',
  userId: 'userId',
  friends: [],
);
const mockedUserProfileDtoWithFriends = UserProfileDto(
  email: 'email',
  name: 'Janusz',
  lastName: 'Nowak',
  avatar: 'avatar',
  userId: 'userId',
  friends: [
    mockedFriedDto1,
    mockedFriedDto2,
    mockedFriedDto3,
  ],
);
const mockedUserProfileDtoWithFriends1 = UserProfileDto(
  email: 'email',
  name: 'Janusz',
  lastName: 'Nowak',
  avatar: 'avatar',
  userId: 'userId',
  friends: [
    mockedFriedDto1,
    mockedFriedDto3,
  ],
);
const mockedLoginDto = LoginDto(
  email: 'a@.com',
  password: 'olasusel123',
);

const mockedLoginEntity = LoginEntity(
  email: 'a@.com',
  password: 'olasusel123',
);

const mockedCreateUserDto = CreateUserDto(
  email: 'a@gmail.com',
  password: 'olasusel123',
);
const mockedCreateUserEntity = CreateUserEntity(
  email: 'a@gmail.com',
  password: 'olasusel123',
);

const mockedFriedEntity = FriendEntity(
  name: 'name',
  lastName: 'lastName',
  avatar: 'avatar',
  userId: 'userId',
);
const mockedFriedEntity1 = FriendEntity(
  name: 'A',
  lastName: 'B',
  avatar: 'avatar',
  userId: 'friend1',
);
const mockedFriedEntity2 = FriendEntity(
  name: 'C',
  lastName: 'D',
  avatar: 'avatar',
  userId: 'friend2',
);
const mockedFriedEntity3 = FriendEntity(
  name: 'E',
  lastName: 'F',
  avatar: 'avatar',
  userId: 'friend3',
);
const mockedFriedDto = FriendDto(
  name: 'name',
  lastName: 'lastName',
  avatar: 'avatar',
  userId: 'userId',
);
const mockedFriedDto1 = FriendDto(
  name: 'A',
  lastName: 'B',
  avatar: 'avatar',
  userId: 'friend1',
);
const mockedFriedDto2 = FriendDto(
  name: 'C',
  lastName: 'D',
  avatar: 'avatar',
  userId: 'friend2',
);
const mockedFriedDto3 = FriendDto(
  name: 'E',
  lastName: 'F',
  avatar: 'avatar',
  userId: 'friend3',
);

final mockedMessageEntity = MessageEntity(
  content: 'content',
  date: DateTime.parse('2023-01-01'),
  senderId: 'senderId',
);
final mockedMessageEntity1 = MessageEntity(
  content: 'content1',
  date: DateTime.parse('2023-01-01'),
  senderId: 'senderId',
);
final mockedMessageDto = MessageDto(
  content: 'content',
  date: DateTime.parse('2023-01-01'),
  senderId: 'senderId',
);
final mockedMessageDto1 = MessageDto(
  content: 'content1',
  date: DateTime.parse('2023-02-01'),
  senderId: 'senderId',
);

final mockedChatTileEntity = ChatTileEntity(
  lastMessage: 'lastMessage',
  lastMessageDate: DateTime.parse('2023-01-01'),
  chatId: 'chatId',
  friendEntity: FriendEntity.fromDto(mockedFriedDto),
);

final mockedChatTileEntity1 = ChatTileEntity(
  lastMessage: 'AA',
  lastMessageDate: DateTime.parse('2023-01-01'),
  chatId: 'chatId',
  friendEntity: FriendEntity.fromDto(mockedFriedDto1),
);

final mockedChatTileEntity2 = ChatTileEntity(
  lastMessage: 'BB',
  lastMessageDate: DateTime.parse('2023-01-01'),
  chatId: 'chatId',
  friendEntity: FriendEntity.fromDto(mockedFriedDto2),
);

final mockedChatTileEntity3 = ChatTileEntity(
  lastMessage: 'CC',
  lastMessageDate: DateTime.parse('2023-01-01'),
  chatId: 'chatId',
  friendEntity: FriendEntity.fromDto(mockedFriedDto3),
);

final mockedConversationDetailsDto1 = ConversationDetailsDto(
  lastMessage: 'AA',
  lastMessageDate: DateTime.parse('2023-01-01'),
  chatId: 'chatId',
  participants: ['userId', 'friend1'],
);
final mockedConversationDetailsDto2 = ConversationDetailsDto(
  lastMessage: 'BB',
  lastMessageDate: DateTime.parse('2023-01-01'),
  chatId: 'chatId',
  participants: ['userId', 'friend2'],
);

final mockedConversationDetailsDto3 = ConversationDetailsDto(
  lastMessage: 'CC',
  lastMessageDate: DateTime.parse('2023-01-01'),
  chatId: 'chatId',
  participants: ['userId', 'friend3'],
);

final mockedConversationDetailsEntity = ConversationDetailsEntity(
  lastMessage: 'lastMessage',
  lastMessageDate: DateTime.parse('2023-01-01'),
  chatId: 'chatId',
  participants: [],
);

const mockedDoubleParamsEntity = DoubleParamsEntity(
  name: 'aaa',
  lastName: 'aaa',
);

final mockedConversationDetailsDtoList = [
  mockedConversationDetailsDto1,
  mockedConversationDetailsDto2,
  mockedConversationDetailsDto3,
];

final mockedConversationDetailsEntityList = [mockedConversationDetailsEntity];
final mockedMessageDtoList = [mockedMessageDto, mockedMessageDto1];
final mockedFriendDtoList = [mockedFriedDto, mockedFriedDto1, mockedFriedDto2];
final mockedChatTileEntityList = [mockedChatTileEntity, mockedChatTileEntity1, mockedChatTileEntity2];
final List<MessageEntity> mockedMessageEntityList = [mockedMessageEntity, mockedMessageEntity1];
final mockedFriendEntityList = [mockedFriedEntity, mockedFriedEntity1, mockedFriedEntity2];

final mockedChatTileEntityListResult = [
  mockedChatTileEntity1,
  mockedChatTileEntity2,
  mockedChatTileEntity3,
];

const sendMessageParams = SendMessageParams(friendId: 'friendId', message: 'halo');
