import 'package:chatterbox/domain/data_source/authentication_remote_source/authentication_data_source.dart';
import 'package:chatterbox/domain/data_source/chat_data_source/chat_data_source.dart';
import 'package:chatterbox/domain/data_source/community_data_source/community_data_source.dart';
import 'package:chatterbox/domain/repositories/authentication_repository/authentication_repository.dart';
import 'package:chatterbox/domain/repositories/chat_data_repository.dart';
import 'package:chatterbox/domain/repositories/community_repository/community_repository.dart';
import 'package:chatterbox/domain/use_case/authentication_use_case/change_password_use_case.dart';
import 'package:chatterbox/domain/use_case/authentication_use_case/get_user_profile_use_case.dart';
import 'package:chatterbox/domain/use_case/authentication_use_case/login_use_case.dart';
import 'package:chatterbox/domain/use_case/authentication_use_case/registration_use_case.dart';
import 'package:chatterbox/domain/use_case/authentication_use_case/sign_out_use_case.dart';
import 'package:chatterbox/domain/use_case/authentication_use_case/user_profile_use_case.dart';
import 'package:chatterbox/domain/use_case/chat_use_case/get_chat_tile_list_use_case.dart';
import 'package:chatterbox/domain/use_case/chat_use_case/get_messages_use_case.dart';
import 'package:chatterbox/domain/use_case/chat_use_case/send_message_use_case.dart';
import 'package:chatterbox/domain/use_case/community_use_case/add_friend_use_case.dart';
import 'package:chatterbox/domain/use_case/community_use_case/delete_friend_use_case.dart';
import 'package:chatterbox/domain/use_case/community_use_case/get_friends_list_use_case.dart';
import 'package:chatterbox/domain/use_case/community_use_case/get_users_use_case.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:mocktail/mocktail.dart';

class MockGetUserProfileUseCase extends Mock implements GetUserProfileUseCase {}

class MockSignOutUseCase extends Mock implements SignOutUseCase {}

class MockChatDataRepository extends Mock implements ChatDataRepository {}

class MockCommunityRepository extends Mock implements CommunityRepository {}

class MockAuthenticationDataSource extends Mock implements AuthenticationDataSource {}

class MockUser extends Mock implements User {
  MockUser({required this.uid});

  @override
  final String uid;
}

class MockUserCredential extends Mock implements UserCredential {
  MockUserCredential({this.user});

  @override
  final User? user;
}

class MockCommunityDataSource extends Mock implements CommunityDataSource {}

class MockChatDataSource extends Mock implements ChatDataSource {}

class MockSendMessageUseCase extends Mock implements SendMessageUseCase {}

class MockGetMessagesUseCase extends Mock implements GetMessagesUseCase {}

class MockGetChatTileListUseCase extends Mock implements GetChatTileListUseCase {}

class MockLoginUseCase extends Mock implements LoginUseCase {}

class MockRegistrationUseCase extends Mock implements RegistrationUseCase {}

class MockGetFriendsListUseCase extends Mock implements GetFriendsListUseCase {}

class MockDeleteFriendUseCase extends Mock implements DeleteFriendUseCase {}

class MockUserProfileUseCase extends Mock implements UserProfileUseCase {}

class MockChangePasswordUseCase extends Mock implements ChangePasswordUseCase {}

class MockGetUsersUseCase extends Mock implements GetUsersUseCase {}

class MockAddFriendUseCase extends Mock implements AddFriendUseCase {}

class MockAuthenticationRepository extends Mock implements AuthenticationRepository {}

class MockFirebaseAuth extends Mock implements FirebaseAuth {
  MockFirebaseAuth({this.currentUser});

  @override
  final User? currentUser;
}

class MockFirebaseException extends Mock implements FirebaseException {
  MockFirebaseException({this.code = 'unknown'});

  @override
  final String code;
}
