import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../domain/entities/chat/message_entity.dart';
import '../../utils/timestamp_converter.dart';

part 'message_dto.freezed.dart';

part 'message_dto.g.dart';

@freezed
class MessageDto with _$MessageDto {
  const factory MessageDto({
    required String content,
    @TimestampConverter() required DateTime date,
    required String senderId,
  }) = _MessageDto;

  factory MessageDto.fromJson(Map<String, dynamic> json) => _$MessageDtoFromJson(json);

  factory MessageDto.fromEntity(MessageEntity entity) {
    return MessageDto(
      content: entity.content,
      date: entity.date,
      senderId: entity.senderId,
    );
  }
}
