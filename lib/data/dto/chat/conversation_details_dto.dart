import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../domain/entities/chat/conversation_details_entity.dart';
import '../../utils/timestamp_converter.dart';

part 'conversation_details_dto.freezed.dart';
part 'conversation_details_dto.g.dart';

@freezed
class ConversationDetailsDto with _$ConversationDetailsDto {
  const factory ConversationDetailsDto({
    required String lastMessage,
    @TimestampConverter() required DateTime lastMessageDate,
    required String chatId,
    required List<String> participants,
  }) = _ConversationDetailsDto;

  factory ConversationDetailsDto.fromJson(Map<String, dynamic> json) => _$ConversationDetailsDtoFromJson(json);

  factory ConversationDetailsDto.fromEntity(ConversationDetailsEntity entity) {
    return ConversationDetailsDto(
      lastMessage: entity.lastMessage,
      lastMessageDate: entity.lastMessageDate,
      chatId: entity.chatId,
      participants: entity.participants,
    );
  }
}
