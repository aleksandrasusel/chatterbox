import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../domain/entities/community_entity/friend_entity.dart';

part 'friend_dto.freezed.dart';

part 'friend_dto.g.dart';

@freezed
class FriendDto with _$FriendDto {
  const factory FriendDto({
    required String name,
    required String lastName,
    required String avatar,
    required String userId,
  }) = _FriendDto;

  factory FriendDto.fromJson(Map<String, dynamic> json) => _$FriendDtoFromJson(json);

  factory FriendDto.fromEntity(FriendEntity entity) {
    return FriendDto(
      name: entity.name,
      lastName: entity.lastName,
      avatar: entity.avatar,
      userId: entity.userId,
    );
  }
}
