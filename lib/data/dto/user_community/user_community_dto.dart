import 'package:freezed_annotation/freezed_annotation.dart';

import '../chat/message_dto.dart';
import 'friend_dto.dart';

part 'user_community_dto.freezed.dart';

part 'user_community_dto.g.dart';

@freezed
class UserCommunityDto with _$UserCommunityDto {
  const factory UserCommunityDto({
    required List<FriendDto> friends,
    required List<MessageDto> messages,
    required MessageDto message,
  }) = _FriendsListDto;

  factory UserCommunityDto.fromJson(Map<String, dynamic> json) => _$UserCommunityDtoFromJson(json);
}
