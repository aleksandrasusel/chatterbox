import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../domain/entities/authentication/user_profile_entity.dart';
import '../user_community/friend_dto.dart';

part 'user_profile_dto.freezed.dart';

part 'user_profile_dto.g.dart';

@freezed
class UserProfileDto with _$UserProfileDto {
  const factory UserProfileDto({
    required String email,
    required String name,
    required String lastName,
    required String avatar,
    required String userId,
    required List<FriendDto> friends,
  }) = _UserProfileDto;

  factory UserProfileDto.fromJson(Map<String, dynamic> json) => _$UserProfileDtoFromJson(json);

  factory UserProfileDto.fromEntity(UserProfileEntity entity) {
    return UserProfileDto(
      email: entity.email,
      name: entity.name,
      lastName: entity.lastName,
      avatar: entity.avatar,
      userId: entity.userId,
      friends: entity.friends,
    );
  }

  factory UserProfileDto.newUser(String email, String userId) => UserProfileDto(
        name: '',
        lastName: '',
        email: email,
        avatar: '',
        friends: [],
        userId: userId,
      );
}
