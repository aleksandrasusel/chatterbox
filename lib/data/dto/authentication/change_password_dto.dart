import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../domain/entities/authentication/change_password_entity.dart';

part 'change_password_dto.freezed.dart';

@freezed
class ChangePasswordDto with _$ChangePasswordDto {
  const factory ChangePasswordDto({
    required String email,
  }) = _ChangePasswordDto;

  factory ChangePasswordDto.fromEntity(ChangePasswordEntity entity) {
    return ChangePasswordDto(
      email: entity.email,
    );
  }
}
