import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:injectable/injectable.dart';

import '../../../domain/data_source/authentication_remote_source/authentication_data_source.dart';
import '../../../domain/utils/exception.dart';
import '../../../domain/utils/success.dart';
import '../../../presentation/utils/enums/errors.dart';
import '../../dto/authentication/create_user_dto.dart';
import '../../dto/authentication/login_dto.dart';
import '../../dto/authentication/user_profile_dto.dart';

@Injectable(as: AuthenticationDataSource)
class AuthenticationRemoteSourceImpl implements AuthenticationDataSource {
  AuthenticationRemoteSourceImpl(this.firestore, this.firebaseAuth);

  final FirebaseFirestore firestore;
  final FirebaseAuth firebaseAuth;

  @override
  Future<UserCredential> createUser(CreateUserDto dto) async {
    try {
      final user = await firebaseAuth.createUserWithEmailAndPassword(
        email: dto.email,
        password: dto.password,
      );

      if (user.user != null) {
        final userId = user.user!.uid;
        await firestore.collection("users").doc(userId).set(
              UserProfileDto.newUser(dto.email, userId).toJson(),
            );
      }

      return user;
    } on FirebaseAuthException catch (e) {
      switch (e.code) {
        case 'weak-password':
          throw ApiException(Errors.weakPassword);
        case 'email-already-in-use':
          throw ApiException(Errors.emailAlreadyExists);
        case 'invalid-email':
          throw ApiException(Errors.invalidEmail);
        case 'operation-not-allowed':
          throw ApiException(Errors.operationNotAllowed);
        case 'unknown':
          throw ApiException(Errors.unknownError);
        default:
          rethrow;
      }
    } catch (e) {
      throw ApiException(Errors.unknownError);
    }
  }

  @override
  Future<User> login(LoginDto dto) async {
    try {
      final result = await firebaseAuth.signInWithEmailAndPassword(email: dto.email, password: dto.password);
      final user = result.user;
      if (user != null) {
        return user;
      } else {
        throw ApiException(Errors.userIsNotFound);
      }
    } on FirebaseException catch (e) {
      switch (e.code) {
        case 'user-disabled':
          throw ApiException(Errors.userDisabled);
        case 'user-not-found':
          throw ApiException(Errors.userIsNotFound);
        case 'invalid-email':
          throw ApiException(Errors.invalidEmail);
        case 'wrong-password':
          throw ApiException(Errors.wrongPassword);
        case 'unknown':
          throw ApiException(Errors.unknownError);
        default:
          rethrow;
      }
    } on ApiException catch (_) {
      rethrow;
    } catch (e) {
      throw ApiException((Errors.unknownError));
    }
  }

  @override
  Future<Success> changePassword(String email) async {
    try {
      await firebaseAuth.sendPasswordResetEmail(email: email);
      return const Success();
    } catch (e) {
      throw ApiException(Errors.invalidEmail);
    }
  }

  @override
  Future<Success> updateUserProfile(UserProfileDto dto) async {
    try {
      final userId = firebaseAuth.currentUser?.uid;
      if (userId != null) {
        final doc = await firestore.collection("users").doc(userId).get();
        if (doc.exists) {
          final UserProfileDto existingUserDoc = UserProfileDto.fromJson(doc.data()!);
          final updatedUserDoc = existingUserDoc.copyWith(
            name: dto.name.isNotEmpty ? dto.name : existingUserDoc.name,
            lastName: dto.lastName.isNotEmpty ? dto.lastName : existingUserDoc.lastName,
          );
          await firestore.collection("users").doc(userId).set(updatedUserDoc.toJson());
        } else {
          await firestore.collection("users").doc(userId).set(dto.toJson());
        }
        return const Success();
      } else {
        throw ApiException(Errors.userIsNotFound);
      }
    } catch (e) {
      throw ApiException(Errors.somethingWentWrong);
    }
  }

  @override
  String getCurrentUserId() {
    final userId = firebaseAuth.currentUser?.uid;
    if (userId != null) {
      return userId;
    } else {
      throw ApiException(Errors.somethingWentWrong);
    }
  }

  @override
  Future<UserProfileDto> getUserProfile() async {
    try {
      final userId = firebaseAuth.currentUser?.uid;

      final doc = await firestore.collection('users').doc(userId).get();
      if (doc.exists && doc.data() != null) {
        final userProfile = UserProfileDto.fromJson(doc.data()!);
        return userProfile;
      } else {
        throw ApiException(Errors.somethingWentWrong);
      }
    } catch (e) {
      throw ApiException(Errors.somethingWentWrong);
    }
  }

  @override
  Future<Success> signOut() async {
    try {
      await firebaseAuth.signOut();
      return const Success();
    } catch (e) {
      throw ApiException(Errors.somethingWentWrong);
    }
  }
}
