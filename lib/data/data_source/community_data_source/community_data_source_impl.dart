import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:injectable/injectable.dart';

import '../../../domain/data_source/community_data_source/community_data_source.dart';
import '../../../domain/utils/exception.dart';
import '../../../domain/utils/success.dart';
import '../../../presentation/utils/enums/errors.dart';
import '../../dto/authentication/user_profile_dto.dart';
import '../../dto/user_community/friend_dto.dart';

@Injectable(as: CommunityDataSource)
class CommunityDataSourceImpl implements CommunityDataSource {
  const CommunityDataSourceImpl(this.firestore, this.firebaseAuth);

  final FirebaseFirestore firestore;
  final FirebaseAuth firebaseAuth;

  String? get userId => firebaseAuth.currentUser?.uid;

  @override
  Future<List<FriendDto>> getUsers() async {
    try {
      final List<FriendDto> usersList = [];
      final docs = await firestore.collection('users').get().then((value) => value.docs);
      for (final doc in docs) {
        final user = FriendDto.fromJson(doc.data());
        usersList.add(user);
      }
      return usersList;
    } catch (e) {
      throw ApiException(Errors.somethingWentWrong);
    }
  }

  @override
  Future<List<FriendDto>> getFriendsList() async {
    try {
      final doc = await firestore.collection('users').doc(userId).get();
      if (doc.exists && doc.data() != null) {
        final user = UserProfileDto.fromJson(doc.data()!);
        final List<FriendDto> userFriends = user.friends;
        return userFriends;
      } else {
        throw ApiException(Errors.somethingWentWrong);
      }
    } catch (e) {
      throw ApiException(Errors.somethingWentWrong);
    }
  }

  @override
  Future<Success> addFriend(FriendDto dto) async {
    try {
      List<FriendDto> currentFriendsList = await getFriendsList();
      List<FriendDto> newFriendsList = [...currentFriendsList, dto];

      await firestore.collection('users').doc(userId).update(
        {
          "friends": newFriendsList.map((friend) => friend.toJson()).toList(),
        },
      );
      return const Success();
    } catch (e) {
      throw ApiException((Errors.somethingWentWrong));
    }
  }

  @override
  Future<List<FriendDto>> deleteFriend(int index) async {
    try {
      final currentFriendsList = await getFriendsList();
      List<FriendDto> newFriendsList = [];
      newFriendsList
        ..addAll(currentFriendsList)
        ..removeAt(index);

      await firestore.collection('users').doc(userId).update(
        {
          "friends": newFriendsList.map((friend) => friend.toJson()).toList(),
        },
      );
      final updatedList = getFriendsList();
      return updatedList;
    } catch (e) {
      throw ApiException((Errors.somethingWentWrong));
    }
  }

  @override
  Future<FriendDto> getFriend(String friendId) async {
    try {
      final doc = await firestore.collection("users").doc(friendId).get();

      final FriendDto friend = FriendDto.fromJson(doc.data()!);
      return friend;
    } catch (e) {
      throw ApiException((Errors.somethingWentWrong));
    }
  }
}
