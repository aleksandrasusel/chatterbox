import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:injectable/injectable.dart';

import '../../../domain/data_source/chat_data_source/chat_data_source.dart';
import '../../../domain/utils/exception.dart';
import '../../../domain/utils/success.dart';
import '../../../presentation/utils/enums/errors.dart';
import '../../dto/chat/conversation_details_dto.dart';
import '../../dto/chat/message_dto.dart';
import '../../utils/chat_id_converter.dart';

@Injectable(as: ChatDataSource)
class ChatDataSourceImpl implements ChatDataSource {
  const ChatDataSourceImpl(this.firestore, this.firebaseAuth);

  final FirebaseFirestore firestore;
  final FirebaseAuth firebaseAuth;

  String? get userId => firebaseAuth.currentUser?.uid;
  static const String _usersChat = "usersChat";
  static const String _chatId = "chatId";
  static const String _messages = "messages";

  Future<Success> _addNewMessageDoc({
    required String message,
    required String chatId,
  }) async {
    try {
      final MessageDto dto = MessageDto(
        content: message,
        date: DateTime.now(),
        senderId: userId!,
      );
      await firestore.collection(_usersChat).doc(chatId).collection(_messages).doc().set(dto.toJson());
      return const Success();
    } catch (e) {
      throw ApiException(Errors.somethingWentWrong);
    }
  }

  @override
  Future<Success> sendMessage(String message, String friendId) async {
    try {
      final String currentParticipants = chatIdConverter(friendId: friendId, userId: userId!);
      final chatDoc = await firestore.collection(_usersChat).where(_chatId, isEqualTo: currentParticipants).get();
      if (chatDoc.docs.isNotEmpty) {
        final chatId = chatDoc.docs.first.id;
        return _addNewMessageDoc(message: message, chatId: chatId);
      } else {
        return _createChatCollection(friendId: friendId, message: message);
      }
    } catch (e) {
      throw ApiException(Errors.somethingWentWrong);
    }
  }

  @override
  Stream<List<MessageDto>> getMessages(String friendId) {
    final String currentParticipants = chatIdConverter(friendId: friendId, userId: userId!);
    final StreamController<List<MessageDto>> controller = StreamController.broadcast();

    final chatStream = firestore.collection(_usersChat).where(_chatId, isEqualTo: currentParticipants).snapshots();
    chatStream.listen((snapshot) {
      if (snapshot.docs.isEmpty) {
        controller.add([]);
      } else {
        final chatDocId = snapshot.docs.first.id;
        firestore.collection(_usersChat).doc(chatDocId).collection(_messages).snapshots().listen(
          (event) {
            List<MessageDto> messages = event.docs.map((doc) => MessageDto.fromJson(doc.data())).toList();
            messages.sort((a, b) => a.date.compareTo(b.date));
            controller.add(messages);
          },
        );
      }
    });

    return controller.stream;
  }

  @override
  Future<List<ConversationDetailsDto>> getChatsList() async {
    try {
      final QuerySnapshot<Map<String, dynamic>> chatMessages =
          await firestore.collection(_usersChat).where("participants", arrayContains: userId!).get();
      if (chatMessages.docs.isNotEmpty) {
        final List<ConversationDetailsDto> list = chatMessages.docs.map((doc) => ConversationDetailsDto.fromJson(doc.data())).toList();
        return list;
      } else {
        return [];
      }
    } catch (e) {
      throw ApiException(Errors.somethingWentWrong);
    }
  }


  Future<Success> _createChatCollection({required String friendId, required String message}) async {
    try {
      final String chatId = chatIdConverter(friendId: friendId, userId: userId!);

      final ConversationDetailsDto conversationDto = ConversationDetailsDto(
        lastMessage: message,
        lastMessageDate: DateTime.now(),
        chatId: chatId,
        participants: [
          friendId,
          userId!,
        ],
      );
      final MessageDto messageDto = MessageDto(
        content: message,
        date: DateTime.now(),
        senderId: userId!,
      );

      final docRef = await firestore.collection(_usersChat).add(conversationDto.toJson());
      await docRef.collection(_messages).add(messageDto.toJson());

      return const Success();
    } catch (e) {
      throw ApiException(Errors.somethingWentWrong);
    }
  }
}
