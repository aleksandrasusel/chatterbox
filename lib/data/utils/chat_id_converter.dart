String chatIdConverter({required String friendId, required String userId}) {
  final result = userId.compareTo(friendId) > 0;
  if (result) {
    return '${userId}_$friendId';
  } else {
    return '${friendId}_$userId';
  }
}
