import 'package:dartz/dartz.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:injectable/injectable.dart';

import '../../../domain/data_source/authentication_remote_source/authentication_data_source.dart';
import '../../../domain/entities/authentication/create_user_entity.dart';
import '../../../domain/entities/authentication/login_entity.dart';
import '../../../domain/entities/authentication/user_profile_entity.dart';
import '../../../domain/repositories/authentication_repository/authentication_repository.dart';
import '../../../domain/utils/exception.dart';
import '../../../domain/utils/failure.dart';
import '../../../domain/utils/success.dart';
import '../../dto/authentication/create_user_dto.dart';
import '../../dto/authentication/login_dto.dart';
import '../../dto/authentication/user_profile_dto.dart';

@Injectable(as: AuthenticationRepository)
class AuthenticationRepositoryImpl implements AuthenticationRepository {
  AuthenticationRepositoryImpl(this._authenticationDataSource);

  final AuthenticationDataSource _authenticationDataSource;

  @override
  Future<Either<Failure, UserCredential>> createUser(
    CreateUserEntity entity,
  ) async {
    try {
      final createUser = await _authenticationDataSource.createUser(CreateUserDto.fromEntity(entity));
      return Right(createUser);
    } on ApiException catch (e) {
      return Left(
        Failure(e.error),
      );
    }
  }

  @override
  Future<Either<Failure, User>> login(LoginEntity entity) async {
    try {
      final user = await _authenticationDataSource.login(LoginDto.fromEntity(entity));

      return Right(user);
    } on ApiException catch (e) {
      return Left(Failure(e.error));
    }
  }

  @override
  Future<Either<Failure, Success>> changePassword(String email) async {
    try {
      await _authenticationDataSource.changePassword(email);
      return const Right(Success());
    } on ApiException catch (e) {
      return Left(Failure(e.error));
    }
  }

  @override
  Future<Either<Failure, Success>> updateUserProfile(UserProfileEntity entity) async {
    try {
      await _authenticationDataSource.updateUserProfile(UserProfileDto.fromEntity(entity));
      return const Right(Success());
    } on ApiException catch (e) {
      return Left(
        Failure(e.error),
      );
    }
  }

  @override
  String getCurrentUserId() {
    try {
      final userId = _authenticationDataSource.getCurrentUserId();
      return userId;
    } on ApiException catch (_) {
      rethrow;
    }
  }

  @override
  Future<Either<Failure, UserProfileEntity>> getUserProfile() async {
    try {
      final doc = await _authenticationDataSource.getUserProfile();
      return Right(UserProfileEntity.fromDto(doc));
    } on ApiException catch (e) {
      return Left(Failure(e.error));
    }
  }

  @override
  Future<Either<Failure, Success>> signOut() async {
    try {
      await _authenticationDataSource.signOut();
      return const Right(Success());
    } on ApiException catch (e) {
      return Left(Failure(e.error));
    }
  }
}
