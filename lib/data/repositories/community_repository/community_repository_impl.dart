import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';

import '../../../domain/data_source/community_data_source/community_data_source.dart';
import '../../../domain/entities/community_entity/friend_entity.dart';
import '../../../domain/repositories/community_repository/community_repository.dart';
import '../../../domain/utils/exception.dart';
import '../../../domain/utils/failure.dart';
import '../../../domain/utils/success.dart';
import '../../dto/user_community/friend_dto.dart';

@Injectable(as: CommunityRepository)
class CommunityRepositoryImpl implements CommunityRepository {
  CommunityRepositoryImpl(this._remoteSource);

  final CommunityDataSource _remoteSource;

  @override
  Future<Either<Failure, List<FriendEntity>>> getUsers() async {
    try {
      final dto = await _remoteSource.getUsers();
      final List<FriendEntity> userList = dto.map((e) => FriendEntity.fromDto(e)).toList();
      return Right(userList);
    } on ApiException catch (e) {
      return Left(
        Failure(e.error),
      );
    }
  }

  @override
  Future<Either<Failure, Success>> addFriend(FriendEntity entity) async {
    try {
      await _remoteSource.addFriend(FriendDto.fromEntity(entity));
      return const Right(Success());
    } on ApiException catch (e) {
      return Left(Failure(e.error));
    }
  }

  @override
  Future<Either<Failure, List<FriendEntity>>> getFriendsList() async {
    try {
      final dto = await _remoteSource.getFriendsList();
      final List<FriendEntity> friendsList = dto.map((e) => FriendEntity.fromDto(e)).toList();
      return Right(friendsList);
    } on ApiException catch (e) {
      return Left(Failure(e.error));
    }
  }

  @override
  Future<Either<Failure, List<FriendEntity>>> deleteFriend(int index) async {
    try {
      final result = await _remoteSource.deleteFriend(index);
      final entity = result.map((e) => FriendEntity.fromDto(e)).toList();
      return Right(entity);
    } on ApiException catch (e) {
      return Left(Failure(e.error));
    }
  }
}
