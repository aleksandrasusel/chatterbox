import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';

import '../../../domain/data_source/authentication_remote_source/authentication_data_source.dart';
import '../../../domain/data_source/chat_data_source/chat_data_source.dart';
import '../../../domain/data_source/community_data_source/community_data_source.dart';
import '../../../domain/entities/chat/chat_tile_entity.dart';
import '../../../domain/entities/chat/message_entity.dart';
import '../../../domain/repositories/chat_data_repository.dart';
import '../../../domain/utils/exception.dart';
import '../../../domain/utils/failure.dart';
import '../../../domain/utils/success.dart';

@Injectable(as: ChatDataRepository)
class ChatDataRepositoryImpl implements ChatDataRepository {
  ChatDataRepositoryImpl(
    this._chatDataSource,
    this._communityDataSource,
    this._authenticationDataSource,
  );

  final AuthenticationDataSource _authenticationDataSource;
  final ChatDataSource _chatDataSource;
  final CommunityDataSource _communityDataSource;

  @override
  Stream<List<MessageEntity>> getChatMessages(String friendId) {
    return _chatDataSource.getMessages(friendId).map((messageDtoList) {
      return messageDtoList.map((messageDto) => MessageEntity.fromDto(messageDto)).toList();
    });
  }

  @override
  Future<Either<Failure, Success>> sendMessage(String message, String friendId) async {
    try {
      await _chatDataSource.sendMessage(message, friendId);
      return const Right(
        Success(),
      );
    } on ApiException catch (e) {
      return Left(
        Failure(e.error),
      );
    }
  }

  @override
  Future<Either<Failure, List<ChatTileEntity>>> getChatsList() async {
    try {
      final userId = _authenticationDataSource.getCurrentUserId();

      final conversationsList = await _chatDataSource.getChatsList();
      final List<ChatTileEntity> chatTileList = [];
      for (final conversation in conversationsList) {
        final friendId = conversation.participants.firstWhere((element) => element != userId);
        final friendDto = await _communityDataSource.getFriend(friendId);
        final chatTileEntity = ChatTileEntity.fromDtos(conversation, friendDto);
        chatTileList.add(chatTileEntity);
      }
      return Right(chatTileList);
    } on ApiException catch (e) {
      return Left(
        Failure(e.error),
      );
    }
  }
}
