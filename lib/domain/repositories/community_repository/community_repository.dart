import 'package:dartz/dartz.dart';

import '../../entities/community_entity/friend_entity.dart';
import '../../utils/failure.dart';
import '../../utils/success.dart';

abstract class CommunityRepository {
  Future<Either<Failure, List<FriendEntity>>> getUsers();

  Future<Either<Failure, Success>> addFriend(FriendEntity entity);

  Future<Either<Failure, List<FriendEntity>>> getFriendsList();

  Future<Either<Failure, List<FriendEntity>>> deleteFriend(int index);
}
