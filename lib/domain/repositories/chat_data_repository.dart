import 'package:dartz/dartz.dart';

import '../entities/chat/chat_tile_entity.dart';
import '../entities/chat/message_entity.dart';
import '../utils/failure.dart';
import '../utils/success.dart';

abstract class ChatDataRepository {

  Stream<List<MessageEntity>> getChatMessages(String friendId);

  Future<Either<Failure, Success>> sendMessage(String message, String friendId);

  Future<Either<Failure, List<ChatTileEntity>>> getChatsList();
}
