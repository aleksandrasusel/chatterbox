import 'package:dartz/dartz.dart';
import 'package:firebase_auth/firebase_auth.dart';

import '../../entities/authentication/create_user_entity.dart';
import '../../entities/authentication/login_entity.dart';
import '../../entities/authentication/user_profile_entity.dart';
import '../../utils/failure.dart';
import '../../utils/success.dart';

abstract class AuthenticationRepository {
  Future<Either<Failure, UserCredential>> createUser(CreateUserEntity entity);

  Future<Either<Failure, User>> login(LoginEntity entity);

  Future<Either<Failure, Success>> signOut();

  Future<Either<Failure, Success>> changePassword(String email);

  Future<Either<Failure, Success>> updateUserProfile(UserProfileEntity entity);

  Future<Either<Failure, UserProfileEntity>> getUserProfile();

  String getCurrentUserId();
}
