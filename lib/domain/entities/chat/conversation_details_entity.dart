import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../data/dto/chat/conversation_details_dto.dart';

part 'conversation_details_entity.freezed.dart';

@freezed
class ConversationDetailsEntity with _$ConversationDetailsEntity {
  const factory ConversationDetailsEntity({
    required String lastMessage,
    required DateTime lastMessageDate,
    required String chatId,
    required List<String> participants,
  }) = _ConversationDetailsEntity;

  factory ConversationDetailsEntity.fromDto(ConversationDetailsDto dto) {
    return ConversationDetailsEntity(
      lastMessage: dto.lastMessage,
      lastMessageDate: dto.lastMessageDate,
      chatId: dto.chatId,
      participants: dto.participants,
    );
  }
}
