import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../data/dto/chat/conversation_details_dto.dart';
import '../../../data/dto/user_community/friend_dto.dart';
import '../community_entity/friend_entity.dart';

part 'chat_tile_entity.freezed.dart';

@freezed
class ChatTileEntity with _$ChatTileEntity {
  const factory ChatTileEntity({
    required DateTime lastMessageDate,
    required String chatId,
    required String lastMessage,
    required FriendEntity friendEntity,
  }) = _ChatTileEntity;

  factory ChatTileEntity.fromDtos(ConversationDetailsDto conversationDetailsDto, FriendDto friendDto) {
    return ChatTileEntity(
      lastMessage: conversationDetailsDto.lastMessage,
      lastMessageDate: conversationDetailsDto.lastMessageDate,
      chatId: conversationDetailsDto.chatId,
      friendEntity: FriendEntity.fromDto(friendDto),
    );
  }
}
