import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../data/dto/chat/message_dto.dart';

part 'message_entity.freezed.dart';

@freezed
class MessageEntity with _$MessageEntity {
  const factory MessageEntity({
    required String content,
    required DateTime date,
    required String senderId,
  }) = _MessageEntity;

  factory MessageEntity.fromDto(MessageDto dto) {
    return MessageEntity(
      content: dto.content,
      date: dto.date,
      senderId: dto.senderId,
    );
  }
}
