import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../data/dto/authentication/user_profile_dto.dart';
import '../../../data/dto/user_community/friend_dto.dart';

part 'user_profile_entity.freezed.dart';

@freezed
class UserProfileEntity with _$UserProfileEntity {
  const factory UserProfileEntity({
    required String email,
    required String name,
    required String lastName,
    required String avatar,
    required String userId,
    required List<FriendDto> friends,
  }) = _UserProfileEntity;

  factory UserProfileEntity.fromDto(UserProfileDto dto) {
    return UserProfileEntity(
      email: dto.email,
      name: dto.name,
      lastName: dto.lastName,
      avatar: dto.avatar,
      userId: dto.userId,
      friends: dto.friends,
    );
  }
}
