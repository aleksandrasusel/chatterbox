import 'package:freezed_annotation/freezed_annotation.dart';

part 'create_user_entity.freezed.dart';

@freezed
class CreateUserEntity with _$CreateUserEntity {
  const factory CreateUserEntity({
    required String email,
    required String password,
  }) = _CreateUserEntity;
}
