import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../data/dto/authentication/change_password_dto.dart';

part 'change_password_entity.freezed.dart';

@freezed
class ChangePasswordEntity with _$ChangePasswordEntity {
  const factory ChangePasswordEntity({
    required String email,
  }) = _ChangePasswordEntity;

  factory ChangePasswordEntity.fromDto(ChangePasswordDto dto) {
    return ChangePasswordEntity(
      email: dto.email,
    );
  }
}
