import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../data/dto/user_community/friend_dto.dart';

part 'friend_entity.freezed.dart';

@freezed
class FriendEntity with _$FriendEntity {
  const factory FriendEntity({
    required String name,
    required String lastName,
    required String avatar,
    required String userId,
  }) = _FriendEntity;

  factory FriendEntity.fromDto(FriendDto dto) {
    return FriendEntity(
      name: dto.name,
      lastName: dto.lastName,
      avatar: dto.avatar,
      userId: dto.userId,
    );
  }
}
