import 'package:freezed_annotation/freezed_annotation.dart';

part 'double_params_entity.freezed.dart';

@freezed
class DoubleParamsEntity with _$DoubleParamsEntity {
  const factory DoubleParamsEntity({
    required String name,
    required String lastName,
  }) = _DoubleParamsEntity;
}
