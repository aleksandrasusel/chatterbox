import 'package:dartz/dartz.dart';

import 'failure.dart';
import 'success.dart';

abstract class UseCase<T, S> {
  Future<Either<Failure, T>> call(S param);
}

abstract class UseCaseSimple<T, S> {
  Future<T> call(S params);
}

abstract class UseCaseSend<S> {
  Future<Either<Failure, Success>> send(S param);
}

abstract class SimpleNoParamsUseCase<T> {
  Future<T> call();
}

abstract class NoParamsUseCaseWithFailure<T> {
  Future<Either<Failure, T>> call();
}

abstract class StreamUseCase<T> {
  Stream<T> call();
}

abstract class StreamParamUseCase<T, S> {
  Stream<T> call(S params);
}
