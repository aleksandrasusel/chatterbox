import 'package:freezed_annotation/freezed_annotation.dart';

import '../../presentation/utils/enums/errors.dart';

part 'failure.freezed.dart';

@freezed
class Failure with _$Failure {
  const factory Failure(
    Errors? appError,
  ) = _Failure;
}
