import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';

import '../../entities/community_entity/friend_entity.dart';
import '../../repositories/community_repository/community_repository.dart';
import '../../utils/failure.dart';
import '../../utils/success.dart';
import '../../utils/use_case.dart';

@injectable
class AddFriendUseCase implements UseCaseSend<FriendEntity> {
  const AddFriendUseCase(this._communityRepository);

  final CommunityRepository _communityRepository;

  @override
  Future<Either<Failure, Success>> send(FriendEntity entity) async {
    return (await _communityRepository.addFriend(entity)).fold(
      (failure) => Left(failure),
      (_) => const Right(Success()),
    );
  }
}
