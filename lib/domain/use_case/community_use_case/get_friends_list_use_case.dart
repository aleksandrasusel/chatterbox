import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';

import '../../entities/community_entity/friend_entity.dart';
import '../../repositories/community_repository/community_repository.dart';
import '../../utils/failure.dart';
import '../../utils/use_case.dart';

@injectable
class GetFriendsListUseCase implements NoParamsUseCaseWithFailure<List<FriendEntity>> {
  const GetFriendsListUseCase(this._communityRepository);

  final CommunityRepository _communityRepository;

  @override
  Future<Either<Failure, List<FriendEntity>>> call() async {
    return (await _communityRepository.getFriendsList()).fold(
      (failure) => Left(failure),
      (entity) => Right(entity),
    );
  }
}
