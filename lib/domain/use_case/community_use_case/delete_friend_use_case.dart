import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';

import '../../entities/community_entity/friend_entity.dart';
import '../../repositories/community_repository/community_repository.dart';
import '../../utils/failure.dart';
import '../../utils/use_case.dart';

@injectable
class DeleteFriendUseCase implements UseCase<List<FriendEntity>, int> {
  const DeleteFriendUseCase(this._communityRepository);

  final CommunityRepository _communityRepository;

  @override
  Future<Either<Failure, List<FriendEntity>>> call(int index) async {
    final result = await _communityRepository.deleteFriend(index);
    return result.fold(
      (l) => Left(l),
      (result) => Right(result),
    );
  }
}
