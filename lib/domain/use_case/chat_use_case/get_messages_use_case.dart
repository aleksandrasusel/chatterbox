import 'package:injectable/injectable.dart';

import '../../entities/chat/message_entity.dart';
import '../../repositories/chat_data_repository.dart';
import '../../utils/use_case.dart';

@injectable
class GetMessagesUseCase implements StreamParamUseCase<List<MessageEntity>, String> {
  const GetMessagesUseCase(this._chatDataRepository);

  final ChatDataRepository _chatDataRepository;

  @override
  Stream<List<MessageEntity>> call(String friendId) {
    return _chatDataRepository.getChatMessages(friendId);
  }
}
