import 'package:dartz/dartz.dart';
import 'package:flutter/cupertino.dart';
import 'package:injectable/injectable.dart';

import '../../repositories/chat_data_repository.dart';
import '../../utils/failure.dart';
import '../../utils/success.dart';
import '../../utils/use_case.dart';

@injectable
class SendMessageUseCase implements UseCaseSend<SendMessageParams> {
  const SendMessageUseCase(this._chatDataRepository);

  final ChatDataRepository _chatDataRepository;

  @override
  Future<Either<Failure, Success>> send(SendMessageParams params) async {
    return (await _chatDataRepository.sendMessage(params.message, params.friendId)).fold(
      (failure) => Left(failure),
      (_) => const Right(Success()),
    );
  }
}

@immutable
class SendMessageParams {
  const SendMessageParams({required this.message, required this.friendId});

  final String message;
  final String friendId;

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is SendMessageParams && other.message == message && other.friendId == friendId;
  }

  @override
  int get hashCode => message.hashCode ^ friendId.hashCode;
}
