import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';

import '../../entities/chat/chat_tile_entity.dart';
import '../../repositories/chat_data_repository.dart';
import '../../utils/failure.dart';
import '../../utils/use_case.dart';

@injectable
class GetChatTileListUseCase implements NoParamsUseCaseWithFailure<List<ChatTileEntity>> {
  const GetChatTileListUseCase(this._chatDataRepository);

  final ChatDataRepository _chatDataRepository;

  @override
  Future<Either<Failure, List<ChatTileEntity>>> call() async {
    return (await _chatDataRepository.getChatsList()).fold(
      (failure) => Left(failure),
      (entity) => Right(entity),
    );
  }
}
