import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';

import '../../repositories/authentication_repository/authentication_repository.dart';
import '../../utils/failure.dart';
import '../../utils/success.dart';
import '../../utils/use_case.dart';

@injectable
class SignOutUseCase implements NoParamsUseCaseWithFailure<Success> {
  const SignOutUseCase(this._authenticationRepository);

  final AuthenticationRepository _authenticationRepository;

  @override
  Future<Either<Failure, Success>> call() async {
    return (await _authenticationRepository.signOut()).fold(
      (failure) => Left(failure),
      (_) => Right(_),
    );
  }
}
