import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';

import '../../entities/authentication/create_user_entity.dart';
import '../../repositories/authentication_repository/authentication_repository.dart';
import '../../utils/failure.dart';
import '../../utils/success.dart';
import '../../utils/use_case.dart';

@injectable
class RegistrationUseCase implements UseCase<Success, CreateUserEntity> {
  const RegistrationUseCase(this._authenticationRepository);

  final AuthenticationRepository _authenticationRepository;

  @override
  Future<Either<Failure, Success>> call(CreateUserEntity entity) async {
    return (await _authenticationRepository.createUser(entity)).fold(
      (failure) => Left(failure),
      (success) => const Right(Success()),
    );
  }
}
