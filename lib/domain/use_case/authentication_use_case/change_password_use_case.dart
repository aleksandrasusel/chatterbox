import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';

import '../../repositories/authentication_repository/authentication_repository.dart';
import '../../utils/failure.dart';
import '../../utils/success.dart';
import '../../utils/use_case.dart';

@injectable
class ChangePasswordUseCase implements UseCaseSend<String> {
  ChangePasswordUseCase(this._authenticationRepository);

  final AuthenticationRepository _authenticationRepository;

  @override
  Future<Either<Failure, Success>> send(String email) async {
    return (await _authenticationRepository.changePassword(email)).fold(
      (failure) => Left(failure),
      (success) => const Right(Success()),
    );
  }
}
