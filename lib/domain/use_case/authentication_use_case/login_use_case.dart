import 'package:dartz/dartz.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:injectable/injectable.dart';

import '../../entities/authentication/login_entity.dart';
import '../../repositories/authentication_repository/authentication_repository.dart';
import '../../utils/failure.dart';
import '../../utils/use_case.dart';

@injectable
class LoginUseCase implements UseCase<User, LoginEntity> {
  const LoginUseCase(this._authenticationRepository);

  final AuthenticationRepository _authenticationRepository;

  @override
  Future<Either<Failure, User>> call(LoginEntity entity) async {
    return (await _authenticationRepository.login(entity)).fold(
      (failure) => Left(failure),
      (user) => Right(user),
    );
  }
}
