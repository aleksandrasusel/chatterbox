import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';

import '../../entities/authentication/user_profile_entity.dart';
import '../../entities/community_entity/double_params_entity.dart';
import '../../repositories/authentication_repository/authentication_repository.dart';
import '../../utils/failure.dart';
import '../../utils/success.dart';
import '../../utils/use_case.dart';

@injectable
class UserProfileUseCase implements UseCase<Success, DoubleParamsEntity> {
  const UserProfileUseCase(this._authenticationRepository);

  final AuthenticationRepository _authenticationRepository;

  @override
  Future<Either<Failure, Success>> call(params) async {
    final userId = _authenticationRepository.getCurrentUserId();
    final entity = UserProfileEntity(
      email: '',
      name: params.name,
      lastName: params.lastName,
      avatar: '',
      userId: userId,
      friends: [],
    );
    return (await _authenticationRepository.updateUserProfile(entity)).fold(
      (failure) => Left(failure),
      (success) => const Right(Success()),
    );
  }
}


