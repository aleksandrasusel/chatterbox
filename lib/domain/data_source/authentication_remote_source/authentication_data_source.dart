import 'package:firebase_auth/firebase_auth.dart';

import '../../../data/dto/authentication/create_user_dto.dart';
import '../../../data/dto/authentication/login_dto.dart';
import '../../../data/dto/authentication/user_profile_dto.dart';
import '../../utils/success.dart';

abstract class AuthenticationDataSource {
  Future<UserCredential> createUser(CreateUserDto dto);

  Future<User> login(LoginDto dto);

  Future<Success> signOut();

  Future<Success> changePassword(String email);

  Future<Success> updateUserProfile(UserProfileDto dto);

  String getCurrentUserId();

  Future<UserProfileDto> getUserProfile();
}
