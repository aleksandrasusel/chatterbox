
import '../../../data/dto/chat/conversation_details_dto.dart';
import '../../../data/dto/chat/message_dto.dart';
import '../../utils/success.dart';

abstract class ChatDataSource {
  Future<Success> sendMessage(String message, String friendId);

  Stream<List<MessageDto>> getMessages(String friendId);

  Future<List<ConversationDetailsDto>> getChatsList();
}
