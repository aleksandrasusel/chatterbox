import '../../../data/dto/user_community/friend_dto.dart';
import '../../utils/success.dart';

abstract class CommunityDataSource {
  Future<List<FriendDto>> getUsers();

  Future<Success> addFriend(FriendDto dto);

  Future<FriendDto> getFriend(String friendId);

  Future<List<FriendDto>> getFriendsList();

  Future<List<FriendDto>> deleteFriend(int index);
}
