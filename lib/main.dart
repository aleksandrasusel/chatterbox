import 'package:auto_route/auto_route.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

import 'firebase_options.dart';
import 'injectable/injectable.dart';
import 'presentation/bloc/app_bloc.dart';
import 'presentation/theme/theme_manager.dart';
import 'presentation/utils/router/app_router.dart';
import 'presentation/utils/translation/generated/l10n.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  configureDependencies();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  runApp(const MyApp());
}

@RoutePage()
class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) => BlocProvider(
        create: (context) => getIt<AppBloc>()..add(const OnCheckUserEvent()),
        child: BlocListener<AppBloc, AppState>(
          listener: (context, state) => state.whenOrNull(
            toHomePage: () => getIt<AppRouter>().push(const HomeRoute()),
            loading: () => const Center(
              child: CircularProgressIndicator(),
            ),
            toIntroPage: () => getIt<AppRouter>().push(const IntroRoute()),
          ),
          child: MaterialApp.router(
            routerDelegate: getIt<AppRouter>().delegate(),
            routeInformationParser: getIt<AppRouter>().defaultRouteParser(),
            supportedLocales: const [
              Locale('en', 'EN'),
            ],
            localizationsDelegates: const [
              Translation.delegate,
              GlobalMaterialLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate,
              GlobalCupertinoLocalizations.delegate,
            ],
            localeResolutionCallback: (locale, supportedLocales) {
              if (locale == null) return supportedLocales.first;

              return supportedLocales.firstWhere(
                (e) => e.languageCode == locale.languageCode && e.countryCode == locale.countryCode,
                orElse: () => supportedLocales.firstWhere(
                  (c) => c.languageCode == locale.languageCode,
                  orElse: () => supportedLocales.first,
                ),
              );
            },
            theme: getIt<ThemeManager>().getTheme(),
          ),
        ),
      );
}
