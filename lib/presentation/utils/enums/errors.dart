import 'package:flutter/material.dart';

enum Errors {
  passwordDoNotMatch,
  weakPassword,
  emailAlreadyExists,
  completeAllFields,
  userIsNotFound,
  wrongPassword,
  invalidEmail,
  userDisabled,
  unknownError,
  operationNotAllowed,
  fieldCanNotBeEmpty,
  somethingWentWrong,
  nointernetConnection,
}

extension ErrorExtension on Errors {
  String errorText(BuildContext context) {
    switch (this) {
      case Errors.passwordDoNotMatch:
        return 'Passwords do not match';
      case Errors.weakPassword:
        return 'Weak password';
      case Errors.emailAlreadyExists:
        return 'Email already exists';
      case Errors.completeAllFields:
        return 'Complete all fields';
      case Errors.userIsNotFound:
        return 'User is not found';
      case Errors.wrongPassword:
        return 'Wrong password';
      case Errors.invalidEmail:
        return 'Invalid email';
      case Errors.userDisabled:
        return 'User disabled';
      case Errors.unknownError:
        return 'Unknown error';
      case Errors.operationNotAllowed:
        return 'Operation not allowed';
      case Errors.fieldCanNotBeEmpty:
        return 'Field can not be empty';
      case Errors.somethingWentWrong:
        return 'Something went wrong';
      case Errors.nointernetConnection:
        return 'No internet connection';
    }
  }
}
