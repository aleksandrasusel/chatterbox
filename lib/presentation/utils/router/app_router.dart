import 'package:auto_route/auto_route.dart';

import 'app_router.gr.dart';

export 'app_router.gr.dart';

@AutoRouterConfig(
  replaceInRouteName: 'Page,Route',
)
class AppRouter extends $AppRouter {
  @override
  RouteType get defaultRouteType => const RouteType.custom(
        transitionsBuilder: TransitionsBuilders.fadeIn,
        durationInMilliseconds: 400,
      );
  @override
  final List<AutoRoute> routes = [
    AutoRoute(page: IntroRoute.page),
    AutoRoute(page: LoginRoute.page),
    AutoRoute(page: RegistrationRoute.page),
    AutoRoute(page: HomeRoute.page),
    AutoRoute(page: ChatRoute.page),
    AutoRoute(page: ProfileSettingRoute.page),
    AutoRoute(page: MyFriendsRoute.page),
    AutoRoute(page: SearchFriendsRoute.page, initial: true),
  ];
}
