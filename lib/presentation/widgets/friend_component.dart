import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

import '../../domain/entities/community_entity/friend_entity.dart';
import '../theme/app_colors.dart';
import '../theme/app_dimensions.dart';
import '../theme/app_paths.dart';
import '../utils/enums/context_extensions.dart';
import 'circle_avatar_widget.dart';

class FriendComponent extends HookWidget {
  const FriendComponent(
    this.iconButton, {
    super.key,
    required this.entity,
  });

  final FriendEntity entity;

  final Widget? iconButton;

  @override
  Widget build(BuildContext context) {
    final textColor = useState(AppColors.limedSpruce);

    return Container(
      width: context.mqs.width * 1,
      height: context.mqs.height * 0.08,
      decoration: const BoxDecoration(
        color: AppColors.white,
        border: Border(
          bottom: BorderSide(color: AppColors.illusion, width: 1),
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          const CircleAvatarWidget(
            avatar: AppPaths.cloudHappy,
            height: 100,
          ),
          Row(
            children: [
              Text(
                entity.name,
                style: TextStyle(
                  color: textColor.value,
                  fontFamily: 'rajdhani',
                  fontSize: AppDimensions.d20,
                ),
              ),
              const SizedBox(
                width: AppDimensions.d10,
              ),
              Text(
                entity.lastName,
                style: TextStyle(
                  color: textColor.value,
                  fontFamily: 'rajdhani',
                  fontSize: AppDimensions.d20,
                ),
              ),
            ],
          ),
          if (iconButton != null) iconButton!,
        ],
      ),
    );
  }
}
