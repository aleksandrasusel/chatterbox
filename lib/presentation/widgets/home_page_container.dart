// ignore: prefer_relative_imports
import 'package:chatterbox/presentation/utils/enums/context_extensions.dart';
import 'package:flutter/material.dart';

import '../theme/app_colors.dart';

class BackgroundContainer extends StatelessWidget {
  const BackgroundContainer({
    super.key,
    this.child,
  });

  final Widget? child;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: context.mqs.width * 0.95,
      height: context.mqs.height * 0.75,
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: AppColors.black.withOpacity(0.7),
            spreadRadius: 2,
            blurRadius: 5,
            offset: const Offset(0, 3),
          ),
        ],
        color: AppColors.white,
        border: Border.all(
          color: AppColors.limedSpruce,
          width: 3,
        ),
        borderRadius: BorderRadius.circular(20),
      ),
      child: child,
    );
  }
}
