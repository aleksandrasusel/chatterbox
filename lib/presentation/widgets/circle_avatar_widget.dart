import 'package:flutter/material.dart';

import '../theme/app_colors.dart';

class CircleAvatarWidget extends StatelessWidget {
  const CircleAvatarWidget({super.key, required this.avatar, required this.height});

  final String avatar;
  final double height;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(5.0),
      child: Container(
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: AppColors.white,
          border: Border.all(
            color: AppColors.limedSpruce,
            width: 2.0,
          ),
        ),
        child: Center(
          child: Image.asset(
            avatar,
            height: height,
          ),
        ),
      ),
    );
  }
}
