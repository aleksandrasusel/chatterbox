part of 'custom_drawer_bloc.dart';

@freezed
class CustomDrawerState with _$CustomDrawerState {
  const factory CustomDrawerState.initial(UserProfileEntity entity) = _Initial;

  const factory CustomDrawerState.loading() = _Loading;

  const factory CustomDrawerState.failure() = _Failure;
}
