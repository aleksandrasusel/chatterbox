part of 'custom_drawer_bloc.dart';

abstract class CustomDrawerEvent extends Equatable {
  const CustomDrawerEvent();

  @override
  List<Object?> get props => [];
}

class OnCustomDrawerInitialEvent extends CustomDrawerEvent {
  const OnCustomDrawerInitialEvent();
}
