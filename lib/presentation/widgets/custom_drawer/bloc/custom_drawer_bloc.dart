import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';

import '../../../../domain/entities/authentication/user_profile_entity.dart';
import '../../../../domain/use_case/authentication_use_case/get_user_profile_use_case.dart';

part 'custom_drawer_bloc.freezed.dart';
part 'custom_drawer_event.dart';
part 'custom_drawer_state.dart';

@injectable
class CustomDrawerBloc extends Bloc<CustomDrawerEvent, CustomDrawerState> {
  CustomDrawerBloc(this._getUserProfileUseCase) : super(const CustomDrawerState.loading()) {
    on<OnCustomDrawerInitialEvent>(_onCustomDrawerInitialEvent);
  }

  final GetUserProfileUseCase _getUserProfileUseCase;

  Future<void> _onCustomDrawerInitialEvent(OnCustomDrawerInitialEvent event, Emitter<CustomDrawerState> emit) async {
    final result = await _getUserProfileUseCase.call();
    result.fold(
      (_) => emit(const CustomDrawerState.failure()),
      (entity) => emit(CustomDrawerState.initial(entity)),
    );
  }
}
