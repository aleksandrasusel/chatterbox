import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../injectable/injectable.dart';
import '../../bloc/app_bloc.dart';
import '../../theme/app_colors.dart';
import '../../theme/app_paths.dart';
import '../../utils/enums/context_extensions.dart';
import '../../utils/router/app_router.dart';
import '../circle_avatar_widget.dart';
import 'bloc/custom_drawer_bloc.dart';

class CustomDrawer extends StatelessWidget {
  const CustomDrawer({super.key});

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: BlocProvider(
        create: (context) => getIt<CustomDrawerBloc>()..add(const OnCustomDrawerInitialEvent()),
        child: BlocConsumer<CustomDrawerBloc, CustomDrawerState>(
          listener: (context, state) => state.maybeWhen(
            orElse: () => const SizedBox.shrink(),
          ),
          builder: (context, state) => state.maybeWhen(
            orElse: () => const SizedBox.shrink(),
            failure: () => const _Body('Brak\ninternetu'),
            initial: (entity) => _Body(entity.name),
          ),
        ),
      ),
    );
  }
}

class _Body extends StatelessWidget {
  const _Body(this.name);

  final String name;

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        DrawerHeader(
          duration: const Duration(milliseconds: 250),
          decoration: const BoxDecoration(
            color: AppColors.illusion,
          ),
          child: Row(
            children: [
              const CircleAvatarWidget(
                avatar: AppPaths.cloudCoffee,
                height: 90,
              ),
              const SizedBox(width: 10),
              name.isEmpty
                  ? GestureDetector(
                      onTap: () => context.router.push(const ProfileSettingRoute()),
                      child: const Icon(
                        Icons.add,
                        color: AppColors.green,
                      ),
                    )
                  : Padding(
                      padding: const EdgeInsets.only(top: 40.0),
                      child: SizedBox(
                        width: context.mqs.width * 0.4,
                        height: context.mqs.height * 0.5,
                        child: Text(
                          name,
                          softWrap: true,
                          style: const TextStyle(
                            color: Colors.white,
                            fontSize: 24,
                          ),
                        ),
                      ),
                    ),
            ],
          ),
        ),
        ListTile(
          title: const Text('Your friends'),
          onTap: () => context.router.push(const MyFriendsRoute()),
        ),
        ListTile(
          title: const Text('Profile settings'),
          onTap: () => context.router.push(const ProfileSettingRoute()),
        ),
        ListTile(
          title: const Text('Back to home Page'),
          onTap: () => context.router.push(const HomeRoute()),
        ),
        ListTile(
          title: const Text('Search a friend'),
          onTap: () => context.router.push(const SearchFriendsRoute()),
        ),
        ListTile(
          title: const Text('Log out'),
          onTap: () => context.read<AppBloc>().add(const OnSignOutEvent()),
        ),
      ],
    );
  }
}
