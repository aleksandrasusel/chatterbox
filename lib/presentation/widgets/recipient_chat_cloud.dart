// ignore: prefer_relative_imports
import 'package:chatterbox/presentation/utils/enums/context_extensions.dart';
import 'package:flutter/material.dart';

import '../theme/app_colors.dart';
import '../theme/app_dimensions.dart';

class RecipientChatCloud extends StatelessWidget {
  const RecipientChatCloud({
    super.key,
    required this.recipient,
  });

  final String recipient;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(AppDimensions.d8),
      child: Container(
        width: context.mqs.width * 0.5,
        height: context.mqs.height * 0.08,
        decoration: BoxDecoration(
          color: AppColors.limedSpruceFog,
          borderRadius: BorderRadius.circular(AppDimensions.d30),
        ),
        child: Padding(
          padding: const EdgeInsets.all(AppDimensions.d16),
          child: Text(
            textAlign: TextAlign.left,
            recipient,
            style: Theme.of(context).textTheme.headlineMedium,
          ),
        ),
      ),
    );
  }
}
