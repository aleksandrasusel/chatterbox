import 'package:flutter/material.dart';

import '../theme/app_colors.dart';
import 'custom_app_bar.dart';
import 'custom_drawer/custom_drawer.dart';
import 'home_page_container.dart';

class AppScaffold extends StatelessWidget {
  const AppScaffold({super.key, required this.body});

  final Widget body;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const CustomDrawer(),
      backgroundColor: AppColors.illusion,
      appBar: const PreferredSize(
        preferredSize: Size.fromHeight(100.0),
        child: CustomAppBar(),
      ),
      body: Center(
        child: BackgroundContainer(
          child: body,
        ),
      ),
    );
  }
}
