// ignore: directives_ordering
import 'package:flutter/material.dart';

import '../theme/app_dimensions.dart';
import '../utils/enums/context_extensions.dart';

class ChatCloud extends StatelessWidget {
  const ChatCloud({
    super.key,
    required this.message,
    required this.color,
    required this.alignment,
  });

  final String message;
  final Color color;
  final Alignment alignment;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(bottom: 16),
      height: context.mqs.height * 0.08,
      decoration: BoxDecoration(
        color: color,
        borderRadius: BorderRadius.circular(AppDimensions.d30),
      ),
      child: Align(
        alignment: alignment,
        child: Padding(
          padding: const EdgeInsets.all(AppDimensions.d18),
          child: Text(
            message,
            style: Theme.of(context).textTheme.labelMedium,
          ),
        ),
      ),
    );
  }
}
