import 'package:flutter/material.dart';

import '../theme/app_colors.dart';

void customSnackBar(
  BuildContext context,
  String text, {
  Color color = AppColors.green,
  int seconds = 1,
}) {
  ScaffoldMessenger.of(context).showSnackBar(
    SnackBar(
      duration: Duration(seconds: seconds),
      backgroundColor: color,
      content: Text(
        text,
      ),
    ),
  );
}
