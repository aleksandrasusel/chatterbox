import 'package:flutter/material.dart';

import '../theme/app_colors.dart';
import '../theme/app_dimensions.dart';
import '../utils/enums/context_extensions.dart';

class CustomTextField extends StatelessWidget {
  const CustomTextField({
    super.key,
    required this.labelText,
    this.icon,
    required this.controller,
    this.padding,
    this.onIconTap,
  });

  final String labelText;
  final TextEditingController controller;
  final IconData? icon;
  final EdgeInsetsGeometry? padding;
  final VoidCallback? onIconTap;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: padding ?? EdgeInsets.zero,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(
            width: context.mqs.width * 0.75,
            height: context.mqs.height * 0.08,
            child: TextField(
              controller: controller,
              decoration: InputDecoration(
                enabledBorder: OutlineInputBorder(
                  borderRadius: icon != null
                      ? const BorderRadius.only(
                          bottomLeft: Radius.circular(AppDimensions.d30),
                          topLeft: Radius.circular(AppDimensions.d30),
                        )
                      : const BorderRadius.all(
                          Radius.circular(AppDimensions.d30),
                        ),
                  borderSide: const BorderSide(color: AppColors.limedSpruce, width: AppDimensions.d2),
                ),
                focusedBorder: const OutlineInputBorder(
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(AppDimensions.d30),
                    topLeft: Radius.circular(AppDimensions.d30),
                  ),
                  borderSide: BorderSide(color: AppColors.illusion, width: AppDimensions.d2),
                ),
                labelText: labelText,
              ),
            ),
          ),
          if (icon != null)
            Padding(
              padding: const EdgeInsets.only(left: AppDimensions.d3),
              child: GestureDetector(
                onTap: onIconTap,
                child: Icon(
                  icon,
                  size: AppDimensions.d44,
                  color: AppColors.illusion,
                ),
              ),
            ),
        ],
      ),
    );
  }
}
