// ignore: prefer_relative_imports
import 'package:chatterbox/presentation/theme/app_dimensions.dart';
import 'package:flutter/material.dart';

import '../theme/app_colors.dart';
import '../theme/app_paths.dart';

class CustomAppBar extends StatelessWidget {
  const CustomAppBar({super.key});

  @override
  Widget build(BuildContext context) {
    return AppBar(
      leading: Padding(
        padding: const EdgeInsets.only(left: AppDimensions.d8),
        child: IconButton(
          onPressed: () => Scaffold.of(context).openDrawer(),
          icon: Image.asset(
            AppPaths.drawer,
          ),
        ),
      ),
      shadowColor: AppColors.black,
      elevation: 7,
      backgroundColor: Colors.white,
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Padding(
            padding: const EdgeInsets.only(
              top: AppDimensions.d20,
              left: AppDimensions.d8,
            ),
            child: Image.asset(
              AppPaths.chatterbox,
              width: 210,
              height: 210,
            ),
          ),
          IconButton(
            iconSize: AppDimensions.d68,
            icon: Image.asset(
              AppPaths.logo,
            ),
            onPressed: () {},
          ),
        ],
      ),
    );
  }
}
