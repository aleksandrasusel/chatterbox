import 'package:flutter/material.dart';

import '../theme/app_colors.dart';

class CustomIconButton extends StatelessWidget {
  const CustomIconButton({super.key, required this.icon, required this.function, required this.size});

  final IconData icon;
  final VoidCallback function;
  final double size;

  @override
  Widget build(BuildContext context) {
    return IconButton(
      onPressed: function,
      icon: Icon(
        icon,
        size: size,
      ),
      color: AppColors.illusion,
    );
  }
}
