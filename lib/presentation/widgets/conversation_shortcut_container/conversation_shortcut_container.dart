import 'package:flutter/material.dart';

import '../../theme/app_colors.dart';
import '../../theme/app_dimensions.dart';
import '../../utils/enums/context_extensions.dart';

class ConversationShortcutContainer extends StatelessWidget {
  const ConversationShortcutContainer({
    super.key,
    required this.name,
    required this.lastName,
    required this.onTap,
    required this.lastMessage,
  });

  final String name;
  final String lastName;
  final String lastMessage;
  final VoidCallback onTap;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(AppDimensions.d4),
      child: GestureDetector(
        onTap: onTap,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(left: AppDimensions.d8),
              child: Container(
                color: AppColors.white,
                height: context.mqs.height * 0.04,
                width: context.mqs.width * 0.89,
                child: Row(
                  children: [
                    Text(
                      name,
                      style: Theme.of(context).textTheme.titleLarge,
                    ),
                    Text(
                      lastName,
                      style: Theme.of(context).textTheme.titleLarge,
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: AppDimensions.d8),
              child: Container(
                height: context.mqs.height * 0.03,
                width: context.mqs.width * 0.89,
                decoration: const BoxDecoration(
                  color: AppColors.white,
                  border: Border(
                    bottom: BorderSide(color: AppColors.illusion, width: AppDimensions.d1),
                  ),
                ),
                child: Text(
                  lastMessage,
                  style: Theme.of(context).textTheme.titleSmall,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
