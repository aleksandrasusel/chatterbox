part of 'app_bloc.dart';

@freezed
class AppState with _$AppState {
  const factory AppState.initial(String user) = _Initial;

  const factory AppState.toIntroPage() = _ToLogin;

  const factory AppState.toHomePage() = _ToHomePage;

  const factory AppState.loading() = _Loading;

  const factory AppState.failure(Errors? error) = _Failure;
}
