import 'dart:async';

import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';

import '../../domain/use_case/authentication_use_case/get_user_profile_use_case.dart';
import '../../domain/use_case/authentication_use_case/sign_out_use_case.dart';
import '../utils/enums/errors.dart';

part 'app_bloc.freezed.dart';
part 'app_event.dart';
part 'app_state.dart';

@injectable
class AppBloc extends Bloc<AppEvent, AppState> {
  AppBloc(this._getUserProfileUseCase, this._signOutUseCase) : super(const AppState.loading()) {
    on<OnCheckUserEvent>(_onCheckUser);
    on<OnSignOutEvent>(_onSignOutEvent);
    on<OnInitializeAppEvent>(_onInitializeAppEvent);
  }

  final GetUserProfileUseCase _getUserProfileUseCase;
  final SignOutUseCase _signOutUseCase;

  Future<void> _onInitializeAppEvent(OnInitializeAppEvent event, Emitter<AppState> emit) async {
    final result = await _getUserProfileUseCase();
    result.fold(
      (l) => emit(const AppState.initial('Nazwa niedostępna')),
      (user) => emit(AppState.initial(user.name)),
    );
  }

  Future<void> _onCheckUser(OnCheckUserEvent event, Emitter<AppState> emit) async {
    final result = await _getUserProfileUseCase();
    result.fold(
      (l) => emit(const AppState.toIntroPage()),
      (r) => emit(const AppState.toHomePage()),
    );
  }

  Future<void> _onSignOutEvent(OnSignOutEvent event, Emitter<AppState> emit) async {
    final result = await _signOutUseCase.call();
    result.fold(
      (l) => emit(AppState.failure(l.appError)),
      (r) => emit(const AppState.toIntroPage()),
    );
  }
}
