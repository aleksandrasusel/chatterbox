part of 'app_bloc.dart';

abstract class AppEvent extends Equatable {
  const AppEvent();

  @override
  List<Object?> get props => [];
}

class OnCheckUserEvent extends AppEvent {
  const OnCheckUserEvent();
}

class OnSignOutEvent extends AppEvent {
  const OnSignOutEvent();
}

class OnInitializeAppEvent extends AppEvent {
  const OnInitializeAppEvent();
}
