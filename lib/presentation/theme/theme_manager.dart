import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';

import 'app_colors.dart';
import 'app_dimensions.dart';

@singleton
class ThemeManager {
  final _themeData = ThemeData(
    brightness: Brightness.light,
    scaffoldBackgroundColor: AppColors.transparent,
    textTheme: const TextTheme(
      titleLarge: TextStyle(
        color: AppColors.limedSpruce,
        fontWeight: FontWeight.w500,
        fontSize: AppDimensions.d20,
        fontFamily: 'rajdhani',
        letterSpacing: 1.5,
      ),
      titleMedium: TextStyle(
        color: AppColors.illusion,
        fontWeight: FontWeight.w400,
        fontSize: AppDimensions.d18,
        fontFamily: 'rajdhani',
        letterSpacing: 1.5,
      ),
      titleSmall: TextStyle(
        color: AppColors.illusion,
        fontWeight: FontWeight.w400,
        fontSize: AppDimensions.d16,
        fontFamily: 'rajdhani',
        letterSpacing: 1.5,
      ),
      displayLarge: TextStyle(
        color: AppColors.illusion,
        fontWeight: FontWeight.w300,
        fontSize: AppDimensions.d20,
        fontFamily: 'rajdhani',
        letterSpacing: 1.5,
      ),
      displayMedium: TextStyle(
        color: AppColors.limedSpruce,
        fontWeight: FontWeight.w300,
        fontSize: AppDimensions.d18,
        fontFamily: 'rajdhani',
        letterSpacing: 1.5,
      ),
      displaySmall: TextStyle(
        color: AppColors.limedSpruce,
        fontWeight: FontWeight.w300,
        fontSize: AppDimensions.d16,
        fontFamily: 'rajdhani',
        letterSpacing: 1.5,
      ),
      bodyLarge: TextStyle(
        color: AppColors.limedSpruce,
        fontWeight: FontWeight.w300,
        fontSize: AppDimensions.d20,
        fontFamily: 'rajdhani',
        letterSpacing: 1.5,
      ),
      bodyMedium: TextStyle(
        color: AppColors.illusion,
        fontWeight: FontWeight.w300,
        fontSize: AppDimensions.d18,
        fontFamily: 'rajdhani',
        letterSpacing: 1.5,
      ),
      bodySmall: TextStyle(
        color: AppColors.illusion,
        fontWeight: FontWeight.w300,
        fontSize: AppDimensions.d16,
        fontFamily: 'rajdhani',
        letterSpacing: 1.5,
      ),
      labelMedium: TextStyle(
        color: AppColors.black,
        fontWeight: FontWeight.w400,
        fontSize: AppDimensions.d16,
        fontFamily: 'rajdhani',
        letterSpacing: 1.5,
      ),
      headlineMedium: TextStyle(
        color: AppColors.white,
        fontWeight: FontWeight.w400,
        fontSize: AppDimensions.d16,
        fontFamily: 'rajdhani',
        letterSpacing: 1.5,
      ),
      headlineLarge: TextStyle(
        color: AppColors.limedSpruce,
        fontWeight: FontWeight.w400,
        fontSize: AppDimensions.d30,
        fontFamily: 'rajdhani',
        letterSpacing: 1.5,
      ),
    ),
    inputDecorationTheme: InputDecorationTheme(
      labelStyle: const TextStyle(
        color: AppColors.limedSpruce,
      ),
      filled: true,
      fillColor: AppColors.white,
      focusColor: AppColors.limedSpruce,
      border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(AppDimensions.d12),
        borderSide: const BorderSide(color: AppColors.limedSpruce),
      ),
      focusedBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(AppDimensions.d12),
        borderSide: const BorderSide(color: AppColors.limedSpruce, width: 2.0),
      ),
      errorBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(AppDimensions.d12),
        borderSide: const BorderSide(color: Colors.red),
      ),
      focusedErrorBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(AppDimensions.d12),
        borderSide: const BorderSide(color: Colors.red, width: 2.0),
      ),
      errorStyle: const TextStyle(
        color: Colors.red,
        fontSize: AppDimensions.d14,
        fontWeight: FontWeight.w400,
      ),
      hintStyle: const TextStyle(
        color: AppColors.limedSpruce,
        fontSize: AppDimensions.d16,
        fontWeight: FontWeight.w300,
      ),
    ),
    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ButtonStyle(
        foregroundColor: MaterialStateProperty.all<Color>(AppColors.limedSpruce),
        backgroundColor: MaterialStateProperty.all<Color>(AppColors.illusion),
        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(
              AppDimensions.d12,
            ),
            side: const BorderSide(
              color: AppColors.limedSpruce,
              width: 1,
            ),
          ),
        ),
        textStyle: MaterialStateProperty.resolveWith<TextStyle>(
          (states) {
            if (states.contains(MaterialState.disabled)) {
              return const TextStyle(
                fontWeight: FontWeight.w300,
                fontSize: AppDimensions.d20,
                fontFamily: 'rajdhani',
                letterSpacing: 1.5,
              );
            } else if (states.contains(MaterialState.pressed)) {
              return const TextStyle(
                fontWeight: FontWeight.w300,
                fontSize: AppDimensions.d24,
                fontFamily: 'rajdhani',
                letterSpacing: 1.5,
              );
            }
            return const TextStyle(
              fontWeight: FontWeight.w300,
              fontSize: AppDimensions.d22,
              fontFamily: 'rajdhani',
              letterSpacing: 1.5,
            );
          },
        ),
        elevation: MaterialStateProperty.all<double>(AppDimensions.d10),
        shadowColor: MaterialStateProperty.all<Color>(AppColors.black),
      ),
    ),
  );

  ThemeData getTheme() => _themeData;
}
