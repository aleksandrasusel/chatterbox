class AppPaths {
  const AppPaths._();

  static const pngPath = 'lib/presentation/assets/png';
  static const svgPath = 'lib/presentation/assets/svg';
  static const iconPath = 'lib/presentation/assets/icons';

  static const String intro = '$pngPath/intro.png';
  static const String cloudCoffee = '$pngPath/cloud_coffee.png';
  static const String cloudHappy = '$pngPath/cloud_happy.png';
  static const String cloudRead = '$pngPath/cloud_read.png';
  static const String cloudGuitar = '$pngPath/cloud_guitar.png';
  static const String cloudShopping = '$pngPath/cloud_shopping.png';
  static const String cloudSmile = '$pngPath/cloud_smile.png';
  static const String background = '$pngPath/background.png';
  static const String arrow = '$iconPath/arrow.png';
  static const String facebook = '$iconPath/facebook.png';
  static const String google = '$iconPath/google.png';
  static const String logo = '$iconPath/logo.png';
  static const String chatterbox = '$iconPath/chatterbox.png';
  static const String drawer = '$iconPath/drawer.png';
  static const String removeUser = '$iconPath/remove_user.png';
  static const String addUser = '$iconPath/add_user.png';
  static const String camera = '$iconPath/camera.png';
}
