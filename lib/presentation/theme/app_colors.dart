import 'package:flutter/material.dart';

/// Color names should be based on http://chir.ag/projects/name-that-color/

class AppColors {
  const AppColors._();

  static const Color black = Colors.black;
  static const Color green = Colors.green;
  static const Color white = Colors.white;
  static const Color grey = Colors.black38;
  static const Color red = Colors.red;
  static const Color transparent = Colors.transparent;
  static const Color illusion = Color(0xFFF6A6BA);
  static const Color illusionFog = Color(0x98F6A6BA);
  static const Color limedSpruce = Color(0xFF3F585C);
  static const Color limedSpruceFog = Color(0xB33F585C);
}
