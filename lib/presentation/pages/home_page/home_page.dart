import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../domain/entities/chat/chat_tile_entity.dart';
import '../../../injectable/injectable.dart';
import '../../theme/app_dimensions.dart';
import '../../theme/app_paths.dart';
import '../../utils/enums/context_extensions.dart';
import '../../utils/router/app_router.dart';
import '../../widgets/app_scaffold.dart';
import '../../widgets/circle_avatar_widget.dart';
import '../../widgets/conversation_shortcut_container/conversation_shortcut_container.dart';
import 'bloc/home_page_bloc.dart';

@RoutePage()
class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return AppScaffold(
      body: BlocProvider(
        create: (context) => getIt<HomePageBloc>()..add(const GetMessagesEvent()),
        child: BlocConsumer<HomePageBloc, HomePageState>(
          listener: (context, state) => state.maybeWhen(
            orElse: () => const SizedBox.shrink(),
          ),
          builder: (context, state) => state.maybeWhen(
            orElse: () => const SizedBox.shrink(),
            initial: (entity) => _Body(chatList: entity),
          ),
        ),
      ),
    );
  }
}

class _Body extends StatelessWidget {
  const _Body({
    required this.chatList,
  });

  final List<ChatTileEntity> chatList;

  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      slivers: [
        SliverToBoxAdapter(
          child: Padding(
            padding: const EdgeInsets.all(AppDimensions.d8),
            child: SizedBox(
              height: context.mqs.height * 0.1,
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: chatList.length,
                itemBuilder: (context, index) => CircleAvatarWidget(
                  avatar: AppPaths.cloudHappy,
                  height: context.mqs.height * 0.9,
                ),
              ),
            ),
          ),
        ),
        SliverList(
          delegate: SliverChildBuilderDelegate(
            childCount: chatList.length,
            (BuildContext context, int index) => ConversationShortcutContainer(
              onTap: () => context.router.push(ChatRoute(friendEntity: chatList[index].friendEntity)),
              name: chatList[index].friendEntity.name,
              lastName: chatList[index].friendEntity.lastName,
              lastMessage: chatList[index].lastMessage,
            ),
          ),
        ),
      ],
    );
  }
}
