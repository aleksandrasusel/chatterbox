part of 'home_page_bloc.dart';

@freezed
class HomePageState with _$HomePageState {
  const factory HomePageState.initial(List<ChatTileEntity> chatList) = _Initial;

  const factory HomePageState.loading() = _Loading;
}
