import 'dart:async';

import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';

import '../../../../domain/entities/chat/chat_tile_entity.dart';
import '../../../../domain/use_case/chat_use_case/get_chat_tile_list_use_case.dart';

part 'home_page_bloc.freezed.dart';
part 'home_page_event.dart';
part 'home_page_state.dart';

@injectable
class HomePageBloc extends Bloc<HomePageEvent, HomePageState> {
  HomePageBloc(
    this._chatTileListUseCase,
  ) : super(const HomePageState.loading()) {
    on<GetMessagesEvent>(_onGetMessagesEvent);
  }

  final GetChatTileListUseCase _chatTileListUseCase;

  Future<void> _onGetMessagesEvent(GetMessagesEvent event, Emitter<HomePageState> emit) async {
    final result = await _chatTileListUseCase.call();
    result.fold(
      (l) => null,
      (r) => emit(HomePageState.initial(r)),
    );
  }
}
