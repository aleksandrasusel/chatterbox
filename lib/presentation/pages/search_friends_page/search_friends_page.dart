import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

import '../../../domain/entities/community_entity/friend_entity.dart';
import '../../../injectable/injectable.dart';
import '../../theme/app_colors.dart';
import '../../theme/app_dimensions.dart';
import '../../utils/enums/context_extensions.dart';
import '../../widgets/app_scaffold.dart';
import '../../widgets/custom_text_field.dart';
import '../../widgets/friend_component.dart';
import 'bloc/search_friends_bloc.dart';
import 'bloc/search_friends_event.dart';
import 'bloc/search_friends_state.dart';

@RoutePage()
class SearchFriendsPage extends StatelessWidget {
  const SearchFriendsPage({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => getIt<SearchFriendsBloc>()..add(const SearchFriendsInitializeEvent()),
      child: BlocListener<SearchFriendsBloc, SearchFriendsState>(
        listener: (context, state) => state.maybeWhen(
          orElse: () => const SizedBox.shrink(),
          success: () => ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(
              content: Text('Wysłano zaproszenie'),
              backgroundColor: AppColors.green,
            ),
          ),
        ),
        child: const _Body(),
      ),
    );
  }
}

class _Body extends HookWidget {
  const _Body();

  @override
  Widget build(BuildContext context) {
    final friendsName = useTextEditingController();
    return BlocBuilder<SearchFriendsBloc, SearchFriendsState>(
      builder: (context, state) {
        return AppScaffold(
          body: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(top: AppDimensions.d20),
                child: CustomTextField(
                  labelText: 'Search a friend',
                  icon: Icons.search_sharp,
                  controller: friendsName,
                ),
              ),
              state.maybeWhen(
                orElse: () => const CircularProgressIndicator(),
                initial: (userList) => Padding(
                  padding: const EdgeInsets.all(AppDimensions.d8),
                  child: SizedBox(
                    width: context.mqs.width * 0.9,
                    height: context.mqs.height * 0.55,
                    child: ListView.builder(
                      itemCount: userList.length,
                      itemBuilder: (context, index) => FriendComponent(
                        IconButton(
                          icon: const Icon(
                            Icons.add,
                            color: AppColors.green,
                          ),
                          onPressed: () => context.read<SearchFriendsBloc>().add(
                                AddFriendEvent(
                                  FriendEntity(
                                    name: userList[index].name,
                                    lastName: userList[index].lastName,
                                    avatar: userList[index].avatar,
                                    userId: userList[index].userId,
                                  ),
                                ),
                              ),
                        ),
                        entity: userList[index],
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
