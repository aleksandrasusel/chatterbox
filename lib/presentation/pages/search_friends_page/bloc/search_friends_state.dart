import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../../domain/entities/community_entity/friend_entity.dart';
import '../../../utils/enums/errors.dart';

part 'search_friends_state.freezed.dart';

@freezed
class SearchFriendsState with _$SearchFriendsState {
  const factory SearchFriendsState.initial(List<FriendEntity> usersList) = _Initial;

  const factory SearchFriendsState.loading() = _Loading;

  const factory SearchFriendsState.success() = _Success;

  const factory SearchFriendsState.failure(Errors error) = _Failure;
}
