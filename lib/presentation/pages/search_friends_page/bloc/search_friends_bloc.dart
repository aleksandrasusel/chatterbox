import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';

import '../../../../domain/use_case/community_use_case/add_friend_use_case.dart';
import '../../../../domain/use_case/community_use_case/get_users_use_case.dart';
import '../../../utils/enums/errors.dart';
import 'search_friends_event.dart';
import 'search_friends_state.dart';

@injectable
class SearchFriendsBloc extends Bloc<SearchFriendsEvent, SearchFriendsState> {
  SearchFriendsBloc(this._communityUseCase, this._addFriendUseCase) : super(const SearchFriendsState.loading()) {
    on<SearchFriendsInitializeEvent>(_initializeEvent);
    on<AddFriendEvent>(_addFriendEvent);
  }

  final GetUsersUseCase _communityUseCase;
  final AddFriendUseCase _addFriendUseCase;

  Future<void> _initializeEvent(SearchFriendsInitializeEvent event, Emitter<SearchFriendsState> emit) async {
    final result = await _communityUseCase();
    result.fold(
      (l) => emit(
        const SearchFriendsState.failure(Errors.somethingWentWrong),
      ),
      (r) => emit(
        SearchFriendsState.initial(r),
      ),
    );
  }

  Future<void> _addFriendEvent(AddFriendEvent event, Emitter<SearchFriendsState> emit) async {
    final result = await _addFriendUseCase.send(event.entity);
    result.fold(
      (l) => emit(const SearchFriendsState.failure(Errors.somethingWentWrong)),
      (r) {
        emit(const SearchFriendsState.success());

        add(const SearchFriendsInitializeEvent());
      },
    );
  }
}
