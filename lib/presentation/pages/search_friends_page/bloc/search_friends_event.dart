import 'package:equatable/equatable.dart';

import '../../../../domain/entities/community_entity/friend_entity.dart';

class SearchFriendsEvent extends Equatable {
  const SearchFriendsEvent();

  @override
  List<Object?> get props => [];
}

class SearchFriendsInitializeEvent extends SearchFriendsEvent {
  const SearchFriendsInitializeEvent();

  @override
  List<Object?> get props => [];
}

class OnSearchEvent extends SearchFriendsEvent {
  const OnSearchEvent();
}

class AddFriendEvent extends SearchFriendsEvent {
  const AddFriendEvent(this.entity);

  final FriendEntity entity;
}
