import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';

import '../../theme/app_colors.dart';
import '../../theme/app_dimensions.dart';
import '../../theme/app_paths.dart';
import '../../utils/enums/context_extensions.dart';
import '../../utils/router/app_router.dart';
import '../../widgets/custom_app_button.dart';

@RoutePage()
class IntroPage extends StatelessWidget {
  const IntroPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Container(
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage(AppPaths.intro),
            fit: BoxFit.cover,
          ),
        ),
        child: Stack(
          children: [
            Positioned(
              right: AppDimensions.d30,
              top: AppDimensions.d40,
              child: CustomAppButton(
                text: context.tr.registration,
                function: () => context.router.push(
                  const RegistrationRoute(),
                ),
              ),
            ),
            Positioned(
              left: AppDimensions.d30,
              bottom: AppDimensions.d40,
              child: ElevatedButton(
                onPressed: () => context.router.push(const LoginRoute()),
                style: Theme.of(context).elevatedButtonTheme.style?.copyWith(
                      foregroundColor: MaterialStateProperty.all<Color>(AppColors.illusion),
                      backgroundColor: MaterialStateProperty.all<Color>(AppColors.limedSpruce),
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(
                            AppDimensions.d12,
                          ),
                          side: const BorderSide(
                            color: AppColors.illusion,
                            width: AppDimensions.d1,
                          ),
                        ),
                      ),
                    ),
                child: Text(context.tr.login),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
