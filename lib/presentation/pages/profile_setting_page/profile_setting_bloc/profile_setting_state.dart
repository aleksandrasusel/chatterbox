part of 'profile_setting_bloc.dart';

@freezed
class ProfileSettingState with _$ProfileSettingState {
  const factory ProfileSettingState.initial() = _Initial;

  const factory ProfileSettingState.success() = _Success;

  const factory ProfileSettingState.loading() = _Loading;

  const factory ProfileSettingState.failure(Errors error) = _Failure;
}
