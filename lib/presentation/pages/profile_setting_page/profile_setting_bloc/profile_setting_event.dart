part of 'profile_setting_bloc.dart';

class ProfileSettingEvent extends Equatable {
  const ProfileSettingEvent();

  @override
  List<Object?> get props => [];
}

class OnTapProfileSettingEvent extends ProfileSettingEvent {
  const OnTapProfileSettingEvent({
    required this.name,
    required this.lastName,
  });

  final String name;
  final String lastName;

  @override
  List<Object?> get props => [
        name,
        lastName,
      ];
}

class OnTapChangePasswordEvent extends ProfileSettingEvent {
  const OnTapChangePasswordEvent({
    required this.currentEmail,
  });

  final String currentEmail;

  @override
  List<Object?> get props => [
        currentEmail,
      ];
}
