import 'dart:async';

import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';

import '../../../../domain/entities/community_entity/double_params_entity.dart';
import '../../../../domain/use_case/authentication_use_case/change_password_use_case.dart';
import '../../../../domain/use_case/authentication_use_case/user_profile_use_case.dart';
import '../../../utils/enums/errors.dart';

part 'profile_setting_bloc.freezed.dart';
part 'profile_setting_event.dart';
part 'profile_setting_state.dart';

@injectable
class ProfileSettingBloc extends Bloc<ProfileSettingEvent, ProfileSettingState> {
  ProfileSettingBloc(this._userDocumentsUseCase, this._changePasswordUseCase) : super(const ProfileSettingState.initial()) {
    on<OnTapProfileSettingEvent>((_onTapProfileSettingEvent));
    on<OnTapChangePasswordEvent>((_onTapChangePassword));
  }

  final UserProfileUseCase _userDocumentsUseCase;
  final ChangePasswordUseCase _changePasswordUseCase;

  Future<void> _onTapProfileSettingEvent(OnTapProfileSettingEvent event, Emitter<ProfileSettingState> emit) async {
    final updateData = await _userDocumentsUseCase(
      DoubleParamsEntity(
        name: event.name,
        lastName: event.lastName,
      ),
    );
    updateData.fold(
      (l) {
        emit(
          const ProfileSettingState.failure(Errors.somethingWentWrong),
        );
      },
      (r) {
        emit(const ProfileSettingState.loading());
        emit(
          const ProfileSettingState.success(),
        );
      },
    );
  }

  Future<void> _onTapChangePassword(OnTapChangePasswordEvent event, Emitter<ProfileSettingState> emit) async {
    final updateData = await _changePasswordUseCase.send(
      event.currentEmail,
    );
    updateData.fold(
      (l) {
        emit(
          ProfileSettingState.failure(l.appError!),
        );
      },
      (r) {
        emit(const ProfileSettingState.loading());
        emit(
          const ProfileSettingState.success(),
        );
      },
    );
  }
}
