import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../injectable/injectable.dart';
import '../../theme/app_colors.dart';
import '../../theme/app_dimensions.dart';
import '../../utils/enums/context_extensions.dart';
import '../../utils/enums/errors.dart';
import '../../utils/router/app_router.gr.dart';
import '../../widgets/app_scaffold.dart';
import '../../widgets/custom_app_button.dart';
import '../../widgets/custom_snack_bar.dart';
import '../../widgets/custom_text_field.dart';
import 'profile_setting_bloc/profile_setting_bloc.dart';

@RoutePage()
class ProfileSettingPage extends StatelessWidget {
  const ProfileSettingPage({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => getIt<ProfileSettingBloc>(),
      child: BlocConsumer<ProfileSettingBloc, ProfileSettingState>(
        listener: (context, state) => state.maybeWhen(
          orElse: () => const SizedBox.shrink(),
          loading: () => const Center(
            child: CircularProgressIndicator(),
          ),
          success: () => context.router.push(
             const HomeRoute(),
          ),
          failure: (error) => customSnackBar(
            context,
            error.errorText(context),
            color: AppColors.red,
          ),
        ),
        builder: (context, state) => state.maybeWhen(
          orElse: () => const SizedBox.shrink(),
          failure: (_) => _Body(),
          initial: () => _Body(),
          loading: () => Container(
            color: AppColors.white,
          ),
        ),
      ),
    );
  }
}

class _Body extends StatelessWidget {
  _Body();

  final name = TextEditingController();
  final lastName = TextEditingController();
  final email = TextEditingController();
  final password = TextEditingController();
  final currentEmail = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return AppScaffold(
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(AppDimensions.d8),
            child: SizedBox(
              height: context.mqs.height * 0.09,
              width: context.mqs.width * 0.9,
              child: Text(
                'Profile settings',
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.headlineLarge,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(AppDimensions.d8),
            child: CustomTextField(
              labelText: "Change your name",
              controller: name,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(AppDimensions.d8),
            child: CustomTextField(
              labelText: "Change your last name",
              controller: lastName,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(AppDimensions.d8),
            child: TextButton(
              child: Text(
                "Change your avatar",
                style: context.tht.bodyLarge,
              ),
              onPressed: () {},
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(AppDimensions.d8),
            child: TextButton(
              child: Text(
                "Change your password",
                style: context.tht.bodyLarge,
              ),
              onPressed: () => _onChangePasswordPressed(context),
            ),
          ),
          SizedBox(
            height: context.mqs.height * 0.1,
          ),
          CustomAppButton(
            text: "Save changes",
            function: () => context.read<ProfileSettingBloc>().add(
                  OnTapProfileSettingEvent(
                    name: name.text,
                    lastName: lastName.text,
                  ),
                ),
          ),
        ],
      ),
    );
  }

  Future<void> _onChangePasswordPressed(BuildContext context) async {
    return showDialog(
      context: context,
      builder: (ctx) => Dialog(
        shadowColor: AppColors.black,
        backgroundColor: AppColors.white,
        insetPadding: const EdgeInsets.symmetric(
          horizontal: AppDimensions.d26,
          vertical: AppDimensions.d100,
        ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Spacer(),
            Padding(
              padding: const EdgeInsets.all(AppDimensions.d8),
              child: Text(
                "Enter your e-mail.\n You'll receive an email with a link to change your password.",
                style: ctx.tht.titleLarge,
                textAlign: TextAlign.center,
              ),
            ),
            const Spacer(),
            Padding(
              padding: const EdgeInsets.all(AppDimensions.d8),
              child: CustomTextField(
                labelText: "Your e-mail",
                controller: currentEmail,
              ),
            ),
            const Spacer(),
            CustomAppButton(
              text: "Save changes",
              function: () => context.read<ProfileSettingBloc>().add(
                    OnTapChangePasswordEvent(
                      currentEmail: currentEmail.text,
                    ),
                  ),
            ),
            const Spacer(),
          ],
        ),
      ),
    );
  }
}
