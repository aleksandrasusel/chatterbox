import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../domain/entities/chat/message_entity.dart';
import '../../../domain/entities/community_entity/friend_entity.dart';
import '../../../injectable/injectable.dart';
import '../../theme/app_colors.dart';
import '../../theme/app_dimensions.dart';
import '../../theme/app_paths.dart';
import '../../utils/enums/context_extensions.dart';
import '../../widgets/app_scaffold.dart';
import '../../widgets/circle_avatar_widget.dart';
import '../../widgets/custom_text_field.dart';
import '../../widgets/sender_chat_cloud.dart';
import 'bloc/chat_bloc.dart';
import 'bloc/chat_event.dart';
import 'bloc/chat_state.dart';

@RoutePage()
class ChatPage extends StatelessWidget {
  const ChatPage({
    super.key,
    required this.friendEntity,
  });

  final FriendEntity friendEntity;

  @override
  Widget build(BuildContext context) {
    return AppScaffold(
      body: BlocProvider(
        create: (context) => getIt<ChatBloc>()..add(InitializeChatEvent(friendId: friendEntity.userId)),
        child: BlocConsumer<ChatBloc, ChatState>(
          listener: (context, state) => state.maybeWhen(
            orElse: () => const SizedBox.shrink(),
            success: () => ScaffoldMessenger.of(context).showSnackBar(
              const SnackBar(
                content: Text('Your message has been sent'),
                backgroundColor: AppColors.green,
              ),
            ),
          ),
          builder: (context, state) => state.maybeWhen(
            orElse: () => const SizedBox.shrink(),
            loaded: (messages, entity) => _Body(
              messages: messages,
              entity: entity,
              friendEntity: friendEntity,
            ),
            loading: () => const Center(child: CircularProgressIndicator()),
          ),
        ),
      ),
    );
  }
}

class _Body extends StatelessWidget {
  const _Body({
    required this.messages,
    required this.entity,
    required this.friendEntity,
  });

  final List<String> messages;
  final List<MessageEntity> entity;
  final FriendEntity friendEntity;

  @override
  Widget build(BuildContext context) {
    final TextEditingController message = TextEditingController();
    return Column(
      children: [
        SizedBox(
          width: context.mqs.width * 1,
          height: context.mqs.height * 0.1,
          child: Row(
            children: [
              const Padding(
                padding: EdgeInsets.all(AppDimensions.d8),
                child: CircleAvatarWidget(
                  avatar: AppPaths.cloudCoffee,
                  height: AppDimensions.d100,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(AppDimensions.d8),
                child: Text(
                  friendEntity.name,
                  style: Theme.of(context).textTheme.titleLarge,
                ),
              ),
            ],
          ),
        ),
        SizedBox(
          width: context.mqs.width * 0.9,
          height: context.mqs.height * 0.5,
          child: ListView.builder(
            itemCount: messages.length,
            itemBuilder: (context, index) {
              return ChatCloud(
                message: messages[index],
                color: entity[index].senderId == friendEntity.userId ? AppColors.illusionFog : AppColors.limedSpruceFog,
                alignment: entity[index].senderId == friendEntity.userId ? Alignment.centerRight : Alignment.centerLeft,
              );
            },
          ),
        ),
        SizedBox(
          height: context.mqs.height * 0.02,
        ),
        Padding(
          padding: const EdgeInsets.all(AppDimensions.d4),
          child: CustomTextField(
            labelText: 'Your message',
            icon: Icons.send,
            controller: message,
            onIconTap: () => context.read<ChatBloc>().add(
                  SendMessageEvent(message: message.text, friendId: friendEntity.userId),
                ),
          ),
        ),
      ],
    );
  }
}
