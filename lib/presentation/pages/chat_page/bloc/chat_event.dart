import 'package:equatable/equatable.dart';

import '../../../../domain/entities/chat/message_entity.dart';

class ChatEvent extends Equatable {
  const ChatEvent();

  @override
  List<Object?> get props => [];
}

class InitializeChatEvent extends ChatEvent {
  const InitializeChatEvent({required this.friendId});

  final String friendId;

  @override
  List<Object?> get props => [];
}

class SendMessageEvent extends ChatEvent {
  const SendMessageEvent({required this.message, required this.friendId});

  final String message;
  final String friendId;
}

class ShowChatMessagesEvent extends ChatEvent {
  const ShowChatMessagesEvent({
    required this.messagesEntity,
    required this.messages,
  });

  final List<String> messages;
  final List<MessageEntity> messagesEntity;
}
