import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';

import '../../../../domain/entities/chat/message_entity.dart';
import '../../../../domain/use_case/chat_use_case/get_messages_use_case.dart';
import '../../../../domain/use_case/chat_use_case/send_message_use_case.dart';
import 'chat_event.dart';
import 'chat_state.dart';

@injectable
class ChatBloc extends Bloc<ChatEvent, ChatState> {
  ChatBloc(this._sendMessageUseCase, this._getMessagesUseCase) : super(const ChatState.loading()) {
    on<InitializeChatEvent>(_onInitializeChatEvent);
    on<SendMessageEvent>(_onSendMessageEvent);
    on<ShowChatMessagesEvent>(_onShowChatMessages);
  }

  StreamSubscription<List<MessageEntity>>? _messagesSubscription;
  final SendMessageUseCase _sendMessageUseCase;
  final GetMessagesUseCase _getMessagesUseCase;

  Future<void> _onInitializeChatEvent(InitializeChatEvent event, Emitter<ChatState> emit) async {
    await _messagesSubscription?.cancel();

    _messagesSubscription = _getMessagesUseCase(event.friendId).listen(
      (messages) {
        final List<String> listMessages = messages.map((e) => e.content).toList();
        final List<MessageEntity> entity = messages;
        add(ShowChatMessagesEvent(messages: listMessages, messagesEntity: entity));
      },
    );
  }

  void _onShowChatMessages(ShowChatMessagesEvent event, Emitter<ChatState> emit) {
    emit(ChatState.loaded(event.messages, event.messagesEntity));
  }

  Future<void> _onSendMessageEvent(SendMessageEvent event, Emitter<ChatState> emit) async {
    await _sendMessageUseCase.send(SendMessageParams(message: event.message, friendId: event.friendId));
  }

  @override
  Future<void> close() {
    _messagesSubscription?.cancel();
    return super.close();
  }
}
