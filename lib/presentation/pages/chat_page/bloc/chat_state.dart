import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../../domain/entities/chat/message_entity.dart';

part 'chat_state.freezed.dart';

@freezed
class ChatState with _$ChatState {
  const factory ChatState.loaded(List<String> messages, List<MessageEntity> entity) = _Loaded;

  const factory ChatState.success() = _Success;

  const factory ChatState.alert() = _Alert;

  const factory ChatState.loading() = _Loading;
}
