import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

import '../../../injectable/injectable.dart';
import '../../theme/app_colors.dart';
import '../../theme/app_dimensions.dart';
import '../../utils/enums/context_extensions.dart';
import '../../utils/router/app_router.gr.dart';
import '../../widgets/app_scaffold.dart';
import '../../widgets/custom_text_field.dart';
import '../../widgets/friend_component.dart';
import 'bloc/my_friends_bloc.dart';
import 'bloc/my_friends_event.dart';
import 'bloc/my_friends_state.dart';

@RoutePage()
class MyFriendsPage extends StatelessWidget {
  const MyFriendsPage({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => getIt<MyFriendsBloc>()..add(const MyFriendsInitializeEvent()),
      child: BlocListener<MyFriendsBloc, MyFriendsState>(
        listener: (context, state) => state.whenOrNull(
          success: () => ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(
              content: Text('Usunięto znajomego'),
              backgroundColor: AppColors.green,
            ),
          ),
          failure: (e) => ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              content: Text(e.name),
              backgroundColor: AppColors.green,
            ),
          ),
        ),
        child: const _Body(),
      ),
    );
  }
}

class _Body extends HookWidget {
  const _Body();

  @override
  Widget build(BuildContext context) {
    final friendsName = useTextEditingController();

    return AppScaffold(
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: AppDimensions.d20),
            child: CustomTextField(
              labelText: 'Search your friend',
              icon: Icons.search_sharp,
              controller: friendsName,
            ),
          ),
          BlocBuilder<MyFriendsBloc, MyFriendsState>(
            builder: (context, state) => state.maybeWhen(
              orElse: () => const CircularProgressIndicator(),
              loaded: (userList) => Padding(
                padding: const EdgeInsets.all(AppDimensions.d8),
                child: SizedBox(
                  width: context.mqs.width * 0.9,
                  height: context.mqs.height * 0.55,
                  child: ListView.builder(
                    itemCount: userList.length,
                    itemBuilder: (context, index) => GestureDetector(
                      onTap: () => context.router.push(ChatRoute(friendEntity: userList[index])),
                      child: FriendComponent(
                        IconButton(
                          icon: const Icon(
                            Icons.remove,
                            color: AppColors.red,
                          ),
                          onPressed: () => context.read<MyFriendsBloc>().add(
                                DeleteFriendEvent(index),
                              ),
                        ),
                        entity: userList[index],

                        // context.read<MyFriendsBloc>().add(
                        //   OnRouteToChatPage(index),
                        // ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
