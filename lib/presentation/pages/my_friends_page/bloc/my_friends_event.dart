import 'package:equatable/equatable.dart';

class MyFriendsEvent extends Equatable {
  const MyFriendsEvent();

  @override
  List<Object?> get props => [];
}

class MyFriendsInitializeEvent extends MyFriendsEvent {
  const MyFriendsInitializeEvent();

  @override
  List<Object?> get props => [];
}

class DeleteFriendEvent extends MyFriendsEvent {
  const DeleteFriendEvent(this.index);

  final int index;
}
