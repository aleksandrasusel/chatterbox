import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';

import '../../../../domain/use_case/community_use_case/delete_friend_use_case.dart';
import '../../../../domain/use_case/community_use_case/get_friends_list_use_case.dart';
import '../../../utils/enums/errors.dart';
import 'my_friends_event.dart';
import 'my_friends_state.dart';

@injectable
class MyFriendsBloc extends Bloc<MyFriendsEvent, MyFriendsState> {
  MyFriendsBloc(
    this._getFriendsListUseCase,
    this._deleteFriendUseCase,
  ) : super(const MyFriendsState.loading()) {
    on<MyFriendsInitializeEvent>(_onInitializeEvent);
    on<DeleteFriendEvent>(_onDeleteFriendEvent);
  }

  final GetFriendsListUseCase _getFriendsListUseCase;
  final DeleteFriendUseCase _deleteFriendUseCase;

  Future<void> _onInitializeEvent(MyFriendsInitializeEvent event, Emitter<MyFriendsState> emit) async {
    final result = await _getFriendsListUseCase.call();
    result.fold(
      (l) => emit(const MyFriendsState.failure(Errors.somethingWentWrong)),
      (r) => emit(
        MyFriendsState.loaded(r),
      ),
    );
  }

  Future<void> _onDeleteFriendEvent(DeleteFriendEvent event, Emitter<MyFriendsState> emit) async {
    final result = await _deleteFriendUseCase.call(event.index);
    result.fold((l) => emit(MyFriendsState.failure(l.appError!)), (r) {
      emit(const MyFriendsState.success());
      emit(MyFriendsState.loaded(r));
    });
  }
}
