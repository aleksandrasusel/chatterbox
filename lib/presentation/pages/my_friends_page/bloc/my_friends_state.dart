import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../../domain/entities/community_entity/friend_entity.dart';
import '../../../utils/enums/errors.dart';

part 'my_friends_state.freezed.dart';

@freezed
class MyFriendsState with _$MyFriendsState {
  const factory MyFriendsState.loaded(List<FriendEntity> usersList) = _Loaded;

  const factory MyFriendsState.loading() = _Loading;

  const factory MyFriendsState.success() = _Success;

  const factory MyFriendsState.failure(Errors error) = _Failure;
}
