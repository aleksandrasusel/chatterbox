part of 'registration_bloc.dart';

class RegistrationEvent extends Equatable {
  const RegistrationEvent();

  @override
  List<Object?> get props => [];
}

class OnTapRegistrationEvent extends RegistrationEvent {
  const OnTapRegistrationEvent({
    required this.password,
    required this.email,
    required this.repeatPassword,
  });

  final String email;
  final String password;
  final String repeatPassword;

  @override
  List<Object?> get props => [password, email];
}
