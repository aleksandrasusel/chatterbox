import 'dart:async';

import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';

import '../../../../domain/entities/authentication/create_user_entity.dart';
import '../../../../domain/use_case/authentication_use_case/registration_use_case.dart';
import '../../../utils/enums/errors.dart';

part 'registration_bloc.freezed.dart';
part 'registration_event.dart';
part 'registration_state.dart';

@injectable
class RegistrationBloc extends Bloc<RegistrationEvent, RegistrationState> {
  RegistrationBloc(this._registrationUseCase) : super(const RegistrationState.initial()) {
    on<OnTapRegistrationEvent>(_onTapRegistration);
  }

  final RegistrationUseCase _registrationUseCase;

  Future<void> _onTapRegistration(OnTapRegistrationEvent event, Emitter<RegistrationState> emit) async {
    if (event.email.isNotEmpty && event.password.isNotEmpty && event.repeatPassword.isNotEmpty) {
      if (event.repeatPassword != event.password) {
        emit(const RegistrationState.failure(Errors.passwordDoNotMatch));
        emit(const RegistrationState.initial());
      } else {
        final registration = await _registrationUseCase(
          CreateUserEntity(
            email: event.email,
            password: event.password,
          ),
        );
        registration.fold(
          (l) {
            emit(RegistrationState.failure(l.appError!));
            emit(const RegistrationState.initial());
          },
          (r) {
            emit(
              const RegistrationState.loading(),
            );
            emit(
              const RegistrationState.success(),
            );
          },
        );
      }
    } else {
      emit(const RegistrationState.failure(Errors.fieldCanNotBeEmpty));
      emit(const RegistrationState.initial());
    }
  }
}
