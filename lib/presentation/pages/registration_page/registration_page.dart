import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../injectable/injectable.dart';
import '../../theme/app_colors.dart';
import '../../theme/app_dimensions.dart';
import '../../theme/app_paths.dart';
import '../../utils/enums/context_extensions.dart';
import '../../utils/enums/errors.dart';
import '../../utils/router/app_router.gr.dart';
import '../../widgets/custom_app_button.dart';
import '../../widgets/custom_snack_bar.dart';
import 'bloc/registration_bloc.dart';

@RoutePage()
class RegistrationPage extends StatelessWidget {
  const RegistrationPage({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => getIt<RegistrationBloc>(),
      child: BlocConsumer<RegistrationBloc, RegistrationState>(
        listener: (context, state) => state.maybeWhen(
          orElse: () => const SizedBox.shrink(),
          loading: () => const Center(
            child: CircularProgressIndicator(),
          ),
          success: () => context.router.push(
            const HomeRoute(),
          ),
          failure: (error) => customSnackBar(
            context,
            error.errorText(context),
            color: AppColors.red,
          ),
        ),
        builder: (context, state) => state.maybeWhen(
          orElse: () => const SizedBox.shrink(),
          failure: (_) => _Body(),
          initial: () => _Body(),
          loading: () => Container(
            color: AppColors.white,
          ),
        ),
      ),
    );
  }
}

class _Body extends StatelessWidget {
  _Body();

  final email = TextEditingController();
  final password = TextEditingController();
  final repeatPassword = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Container(
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage(AppPaths.background),
            fit: BoxFit.cover,
          ),
        ),
        child: Stack(
          children: [
            Positioned(
              left: context.mqs.width * 0.1,
              top: context.mqs.height * 0.2,
              right: context.mqs.width * 0.1,
              child: SizedBox(
                child: Text(
                  context.tr.registration,
                  style: Theme.of(context).textTheme.headlineLarge,
                ),
              ),
            ),
            Positioned(
              left: context.mqs.width * 0.1,
              top: context.mqs.height * 0.35,
              right: context.mqs.width * 0.1,
              child: SizedBox(
                child: TextField(
                  controller: email,
                  decoration: InputDecoration(
                    labelText: context.tr.email,
                  ),
                ),
              ),
            ),
            Positioned(
              left: context.mqs.width * 0.1,
              top: context.mqs.height * 0.45,
              right: context.mqs.width * 0.1,
              child: SizedBox(
                child: TextField(
                  controller: password,
                  obscureText: true,
                  decoration: InputDecoration(
                    labelText: context.tr.password,
                  ),
                ),
              ),
            ),
            Positioned(
              left: context.mqs.width * 0.1,
              top: context.mqs.height * 0.55,
              right: context.mqs.width * 0.1,
              child: SizedBox(
                child: TextField(
                  controller: repeatPassword,
                  obscureText: true,
                  decoration: InputDecoration(
                    labelText: "${context.tr.repeat} ${context.tr.password}",
                  ),
                ),
              ),
            ),
            Positioned(
              left: context.mqs.width * 0.4,
              top: context.mqs.height * 0.64,
              right: context.mqs.width * 0.1,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(AppDimensions.d8),
                    child: GestureDetector(
                      onTap: () => context.router.push(const IntroRoute()),
                      child: Image.asset(
                        AppPaths.google,
                        height: AppDimensions.d26,
                        width: AppDimensions.d26,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(AppDimensions.d8),
                    child: GestureDetector(
                      onTap: () => context.router.push(const IntroRoute()),
                      child: Image.asset(
                        AppPaths.facebook,
                        height: AppDimensions.d26,
                        width: AppDimensions.d26,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Positioned(
              top: context.mqs.height * 0.70,
              left: context.mqs.width * 0.25,
              child: SizedBox(
                width: context.mqs.width * 0.5,
                child: CustomAppButton(
                  text: context.tr.registration,
                  function: () => context.read<RegistrationBloc>().add(
                        OnTapRegistrationEvent(
                          password: password.text,
                          email: email.text,
                          repeatPassword: repeatPassword.text,
                        ),
                      ),
                ),
              ),
            ),
            Positioned(
              bottom: context.mqs.height * 0.1,
              left: context.mqs.height * 0.05,
              child: GestureDetector(
                onTap: () => context.router.push(const IntroRoute()),
                child: Image.asset(
                  AppPaths.arrow,
                  height: AppDimensions.d30,
                  width: AppDimensions.d30,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
