part of 'login_bloc.dart';

class LoginEvent extends Equatable {
  const LoginEvent();

  @override
  List<Object?> get props => [];
}

class OnTapLoginEvent extends LoginEvent {
  const OnTapLoginEvent({
    required this.password,
    required this.email,
  });

  final String password;
  final String email;

  @override
  List<Object?> get props => [password, email];
}
