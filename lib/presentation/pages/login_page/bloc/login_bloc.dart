import 'dart:async';

import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';

import '../../../../domain/entities/authentication/login_entity.dart';
import '../../../../domain/use_case/authentication_use_case/login_use_case.dart';
import '../../../utils/enums/errors.dart';

part 'login_bloc.freezed.dart';

part 'login_event.dart';

part 'login_state.dart';

@injectable
class LoginBloc extends Bloc<LoginEvent, LoginState> {
  LoginBloc(this._loginUseCase) : super(const LoginState.initial()) {
    on<OnTapLoginEvent>(_onTapLogin);
  }

  final LoginUseCase _loginUseCase;

  Future<void> _onTapLogin(OnTapLoginEvent event, Emitter<LoginState> emit) async {
    if (event.email.isNotEmpty && event.password.isNotEmpty) {
      final login = await _loginUseCase(
        LoginEntity(email: event.email, password: event.password),
      );
      login.fold(
        (l) => emit(LoginState.failure(l.appError!)),
        (r) => emit(const LoginState.success()),
      );
    }
  }
}
